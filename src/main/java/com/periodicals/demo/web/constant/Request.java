package com.periodicals.demo.web.constant;

public final class Request {

    public static final String EDITION_ID = "editionId";
    public static final String NOT_ENOUGH_MONEY = "notEnoughMoney";
    public static final String ORDER_DONE = "orderDone";
    public static final String SOMETHING_WRONG = "somethingWrong";
    public static final String ALL_USERS = "allUsers";
    public static final String USER_ID = "userId";
    public static final String USER_IS_BLOCKED = "isBlocked";
    public static final String ALL_GENRES = "allGenres";
    public static final String GENRE_NAME_RU = "genreNameRu";
    public static final String GENRE_NAME_EN = "genreNameEn";
    public static final String GENRE_CURRENT = "genreCurrent";
    public static final String ADD_GENRE_EM_RU = "addGenreErrorMessageRu";
    public static final String ADD_GENRE_EM_EN = "addGenreErrorMessageEn";

    public static final String ADD_EDITION_NAME_EN_EM = "addEditionNameErrorMessageEn";
    public static final String ADD_EDITION_NAME_RU_EM = "addEditionNameErrorMessageRu";
    public static final String ADD_EDITION_PRICE_EM = "addEditionPriceErrorMessage";
    public static final String ADD_EDITION_DATE_EM = "addEditionDateErrorMessage";
    public static final String ADD_EDITION_TITLE_EN_EM = "addEditionTitleErrorMessageEn";
    public static final String ADD_EDITION_TITLE_RU_EM = "addEditionTitleErrorMessageRu";
    public static final String ADD_EDITION_GENRE_EM = "addEditionGenreErrorMessage";

    public static final String ADD_EDITION_CURRENT_EN = "currentEditionEn";
    public static final String ADD_EDITION_CURRENT_RU = "currentEditionRu";
    public static final String EDITION_EXISTS = "editionExists";

    public static final String SET_LOCALE = "setLocale";

    public static final String ERROR_MSG = "errorMessage";

    // Pagination
    public static final String PAGE_QUANTITY = "pageQuantity";
    public static final String PAGE = "page";

    public static final String MAIN = "main";

}
