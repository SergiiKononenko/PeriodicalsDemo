package com.periodicals.demo.web.constant;

public final class Session {

    // View vars:
    public static final String USER = "user";
    public static final String ALL_IN_CART = "allInCart";
    public static final String CART_TOTAL_COST = "cartTotalCost";
    public static final String FOUND_EDITIONS = "foundEditions";
    public static final String ALL_ORDERS = "allOrders";
    public static final String PAGINATION_PAGE = "paginationPage";

    // Technical vars:
    public static final String ALL_EDITIONS = "allEditions";
    public static final String LANGUAGE = "language";

}
