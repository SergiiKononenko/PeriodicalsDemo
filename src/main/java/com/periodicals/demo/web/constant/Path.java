package com.periodicals.demo.web.constant;

public final class Path {

    // Opened pages
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_REGISTER = "/registration.jsp";
    public static final String PAGE_MAIN = "/index.jsp";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";

    // User pages
    public static final String PAGE_REPLENISH = "/WEB-INF/jsp/replenish.jsp";
    public static final String PAGE_SETTINGS = "/WEB-INF/jsp/settings.jsp";
    public static final String PAGE_SUBSCRIPTIONS = "/WEB-INF/jsp/subscriptions.jsp";
    public static final String PAGE_CART = "/WEB-INF/jsp/cart.jsp";

    // Admin pages
    public static final String PAGE_USERS = "/WEB-INF/jsp/admin/users.jsp";
    public static final String PAGE_ADD_EDITION = "/WEB-INF/jsp/admin/add_edition.jsp";
    public static final String PAGE_ADD_GENRE = "/WEB-INF/jsp/admin/add_genre.jsp";
    public static final String PAGE_EDIT_EDITION = "/WEB-INF/jsp/admin/edit_edition.jsp";

    // User commands
    public static final String COMMAND_REPLENISH_PAGE = "controller?command=replenishPage";
    public static final String COMMAND_SETTINGS_PAGE = "controller?command=settingsPage";
    public static final String COMMAND_MY_SUBSCRIPTIONS = "controller?command=subscriptionsPage";
    public static final String COMMAND_CART_PAGE = "controller?command=cartPage";

    public static final String COMMAND_MAIN_PAGE = "controller?command=mainPage";

    // Admin commands
    public static final String COMMAND_LIST_USERS = "controller?command=listUsers";
    public static final String COMMAND_ADD_EDITION = "controller?command=addEditionPage";
    public static final String COMMAND_ADD_GENRE = "controller?command=addGenrePage";

}
