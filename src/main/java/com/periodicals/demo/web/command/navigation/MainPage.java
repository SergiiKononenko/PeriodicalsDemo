package com.periodicals.demo.web.command.navigation;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.Pagination;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Main page controller.
 *
 * @author KSV
 */

public class MainPage implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        List<Edition> allEditions = (List<Edition>) session.getAttribute(Session.ALL_EDITIONS);
        // Get all editions from the database
        if (allEditions == null || request.getParameter(Request.MAIN) != null) {
            String language = (String) session.getAttribute(Session.LANGUAGE);
            allEditions = EditionDAO.getInstance().getAllEditions(language);
            session.setAttribute(Session.ALL_EDITIONS, allEditions);
        }

        Pagination.setUpPagination(request, allEditions);

        return Path.PAGE_MAIN;
    }

}
