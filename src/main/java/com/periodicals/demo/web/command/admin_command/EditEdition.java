package com.periodicals.demo.web.command.admin_command;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class EditEdition implements ICommand {

    private static final Logger log = Logger.getLogger(AddEdition.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_EDIT_EDITION;
        log.debug("Validate all fields and create new Edition");
        List<Edition> newEditions = AddEdition.createEditions(request);
        if (newEditions == null) {
            return forward;
        }
        // If editions exists, set ID to update DB by ID
        String editionIdStr = request.getParameter(Request.EDITION_ID);

        if (editionIdStr != null && !"".equals(editionIdStr)) {
            int editionIdInt = Integer.parseInt(editionIdStr);
            newEditions.get(0).setId(editionIdInt);

            if (EditionDAO.getInstance().updateEdition(newEditions)) {
                try {
                    log.debug("Edit success, update data and send redirect to current page");
                    HttpSession session = request.getSession();
                    String language = (String) session.getAttribute(Session.LANGUAGE);
                    // Update content (reload all editions to session after update one of them)
                    List<Edition> allEditions = EditionDAO.getInstance().getAllEditions(language);
                    request.getSession().setAttribute(Session.ALL_EDITIONS, allEditions);

                    response.sendRedirect(Path.COMMAND_MAIN_PAGE);
                    forward = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return forward;
    }

}
