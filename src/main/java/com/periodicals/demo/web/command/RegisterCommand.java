package com.periodicals.demo.web.command;

import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.command.util.FieldsValidator;
import com.periodicals.demo.web.command.util.PasswordEncryption;
import com.periodicals.demo.web.constant.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterCommand implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter("login");
        String pass = request.getParameter("password");
        String errorMassage;
        String forward = Path.PAGE_REGISTER;

        if (!FieldsValidator.isLoginPageFieldsValid(request, login, pass)) {
            return forward;
        }

        User user = UserDAO.getInstance().findUserByLogin(login);

        if (user != null && login.equals(user.getLogin())) {
            errorMassage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.unique_login");
            request.setAttribute("notUniqueLogin", errorMassage);
            return forward;
        } else {
            pass = PasswordEncryption.md5String(pass);
            user = new User(login, pass);
            if (UserDAO.getInstance().createUser(user)) {
                request.setAttribute("registerSuccess", ErrorMassageFinder.getResourceBundle(request).getString("success_msg.registration_successful"));
                forward = Path.PAGE_LOGIN;
            } else {
                errorMassage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.server_error");
                request.setAttribute("somethingWrongRegMassage", errorMassage);
            }
        }
        return forward;
    }

}
