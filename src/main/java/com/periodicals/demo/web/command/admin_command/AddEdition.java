package com.periodicals.demo.web.command.admin_command;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddEdition implements ICommand {

    private static final Logger log = Logger.getLogger(AddEdition.class);
    private static final String ENGLISH = "en";
    private static final String RUSSIAN = "ru";

    public static List<Edition> createEditions(HttpServletRequest request) {
        String editionNameEn = request.getParameter("editionNameEn");
        String editionNameRu = request.getParameter("editionNameRu");
        String price = request.getParameter("price");
        String date = request.getParameter("date");
        String titleEn = request.getParameter("titleEn");
        String titleRu = request.getParameter("titleRu");
        String genre = request.getParameter("genre");

        // Validate fields
        if (!isAddEditionFieldsValid(request, editionNameEn, editionNameRu, price, date, titleEn, titleRu, genre)) {
            return null;
        }
        // If fields is ok, than create 2 editions: en and ru
        Edition englishVersionEdition = new Edition(0, editionNameEn, titleEn, Double.parseDouble(price), null, genre);
        Edition russianVersionEdition = new Edition(0, editionNameRu, titleRu, 0, null, null);

        // String to date:
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date editionDate = format.parse(date);
            englishVersionEdition.setPubDate(editionDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Edition> editionsList = new ArrayList<>();
        editionsList.add(englishVersionEdition);
        editionsList.add(russianVersionEdition);

        return editionsList;
    }

    public static boolean isAddEditionFieldsValid(HttpServletRequest request, String editionNameEn, String editionNameRu,
                                                  String price, String date, String titleEn, String titleRu, String genre) {
        boolean isValid = true;

        // Check fields
        if (!isEditionEnglishNameValid(editionNameEn, request)) {
            isValid = false;
        } else if (!isEditionRussianNameValid(editionNameRu, request)) {
            isValid = false;
        } else if (!isEditionPriceValid(price, request)) {
            isValid = false;
        } else if (!isEditionDateValid(date, request)) {
            isValid = false;
        } else if (!isEditionEnglishTitleValid(titleEn, request)) {
            isValid = false;
        } else if (!isEditionRussianTitleValid(titleRu, request)) {
            isValid = false;
        } else if (!isEditionGenreValid(genre, request)) {
            isValid = false;
        }

        return isValid;
    }

    private static boolean isEditionEnglishNameValid(String editionNameEn, HttpServletRequest request) {
        boolean isValid = true;
        if (!editionNameEn.matches("[\\w\\s\\p{P}]{3,20}")) {
            String invalidData = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_edition_name_en");

            request.setAttribute(Request.ADD_EDITION_NAME_EN_EM, invalidData);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionRussianNameValid(String editionNameRu, HttpServletRequest request) {
        boolean isValid = true;
        if (!editionNameRu.matches("[а-яА-ЯёЁ\\s\\d\\p{P}]{3,20}")) {
            String invalidData = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_edition_name_ru");

            request.setAttribute(Request.ADD_EDITION_NAME_RU_EM, invalidData);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionPriceValid(String editionPrice, HttpServletRequest request) {
        boolean isValid = true;

        if (!isEmptyField(editionPrice)) {
            String emptyFieldMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_field");
            request.setAttribute(Request.ADD_EDITION_PRICE_EM, emptyFieldMessage);
            isValid = false;
        } else if (!editionPrice.matches("^[0-9]*[.]?[0-9]+$") || Double.parseDouble(editionPrice) < 0) {
            String invalidData = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_data");
            request.setAttribute(Request.ADD_EDITION_PRICE_EM, invalidData);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionDateValid(String editionDate, HttpServletRequest request) {
        boolean isValid = true;

        if (!isEmptyField(editionDate)) {
            String emptyFieldMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_field");
            request.setAttribute(Request.ADD_EDITION_DATE_EM, emptyFieldMessage);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionEnglishTitleValid(String editionTitleEn, HttpServletRequest request) {
        boolean isValid = true;


        if (!editionTitleEn.matches("[\\w\\p{P}\\s]{20,250}")) {
            String invalidData = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_edition_title_en");

            request.setAttribute(Request.ADD_EDITION_TITLE_EN_EM, invalidData);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionRussianTitleValid(String editionTitleRu, HttpServletRequest request) {
        boolean isValid = true;


        if (!editionTitleRu.matches("[а-яА-ЯёЁ\\p{P}\\s\\d]{20,250}")) {
            String invalidData = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_edition_title_ru");

            request.setAttribute(Request.ADD_EDITION_TITLE_RU_EM, invalidData);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEditionGenreValid(String editionGenre, HttpServletRequest request) {
        boolean isValid = true;

        if (!isEmptyField(editionGenre)) {
            String emptyFieldMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_field");
            request.setAttribute(Request.ADD_EDITION_GENRE_EM, emptyFieldMessage);
            isValid = false;
        }
        return isValid;
    }

    private static boolean isEmptyField(String requestParam) {
        return requestParam != null && !"".equals(requestParam);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        String forward = Path.PAGE_ADD_EDITION;
        log.debug("Validate all fields and create new Edition");

        // Validate and get list of 2 new Edition: En and Ru versions
        List<Edition> newEditionList = createEditions(request);
        if (newEditionList == null) {
            return forward;
        }

        //Check edition for duplicate (In this case list gives 1 edition)
        List<Edition> englishVersion = EditionDAO.getInstance().getAllEditions(ENGLISH);
        List<Edition> russianVersion = EditionDAO.getInstance().getAllEditions(RUSSIAN);
        // First compare english version, than russian
        if (englishVersion.contains(newEditionList.get(0)) || russianVersion.contains(newEditionList.get(1))) {
            request.setAttribute(Request.EDITION_EXISTS, ErrorMassageFinder.getResourceBundle(request).getString("error_msg.edition_exists"));
            return forward;
        }

        if (EditionDAO.getInstance().createEdition(newEditionList)) {
            try {
                log.debug("Creation success, send redirect to current page");
                response.sendRedirect(Path.COMMAND_ADD_EDITION);
                forward = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return forward;
    }

}