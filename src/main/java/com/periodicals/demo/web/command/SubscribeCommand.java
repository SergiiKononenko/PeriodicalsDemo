package com.periodicals.demo.web.command;

import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.OrdersDAO;
import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.EditionFinder;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SubscribeCommand implements ICommand {

    private static final Logger log = Logger.getLogger(SubscribeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_MAIN;
        String editionIdStr = request.getParameter("editionId");
        if (editionIdStr != null && !"".equals(editionIdStr)) {
            int editionIdInt = Integer.parseInt(editionIdStr);

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(Session.USER);

            List<Edition> editionList = (List<Edition>) session.getAttribute(Session.FOUND_EDITIONS);

            Edition currentEdition = EditionFinder.getEditionFromListById(editionIdInt, editionList);

            if (currentEdition != null) {
                if (!isEnoughMoneyToSubscribe(user.getBalance(), currentEdition.getPrice())) {
                    request.setAttribute("notEnoughMoney",
                            ErrorMassageFinder.getResourceBundle(request).getString("error_msg.not_enough_money"));
                    log.debug("Not enough money to subscribe");
                    return forward;
                }
                boolean isOrderDone = OrdersDAO.getInstance().createOrder(user.getId(), currentEdition.getId());
                if (isOrderDone) {
                    user.setBalance(user.getBalance() - currentEdition.getPrice());
                    UserDAO.getInstance().updateUserData(user);
                    log.debug("User balance after subscription: " + user.getBalance());
                    List<Edition> ordersList = (List<Edition>) session.getAttribute(Session.ALL_ORDERS);
                    ordersList.add(currentEdition);
                }
            }

        }
        return forward;
    }

    private boolean isEnoughMoneyToSubscribe(double userBalance, double editionPrice) {
        boolean isEnoughMoneyToSubscribe = false;
        if ((userBalance - editionPrice) >= 0) {
            isEnoughMoneyToSubscribe = true;
        }
        return isEnoughMoneyToSubscribe;
    }

}
