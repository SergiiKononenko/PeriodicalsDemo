package com.periodicals.demo.web.command.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PasswordEncryption {

    public static String md5String(String pass) {
        MessageDigest md;
        byte[] digest = new byte[0];

        try {
            md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(pass.getBytes());
            digest = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger bi = new BigInteger(1, digest);
        StringBuilder md5Hex = new StringBuilder(bi.toString(16));

        while (md5Hex.length() < 32) {
            md5Hex.insert(0, "0");
        }

        return md5Hex.toString();
    }

}
