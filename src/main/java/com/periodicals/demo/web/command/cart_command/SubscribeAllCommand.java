package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.db.OrdersDAO;
import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.SubscribeCommand;
import com.periodicals.demo.web.command.util.EditionFinder;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class SubscribeAllCommand implements ICommand {

    private static final Logger log = Logger.getLogger(SubscribeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("SubscribeAllCommand HERE");
        String forward = Path.PAGE_CART;

        HttpSession session = request.getSession();
        double totalCost = (double) session.getAttribute(Session.CART_TOTAL_COST);
        User currentUser = (User) session.getAttribute(Session.USER);

        if (currentUser.getBalance() - totalCost < 0) {
            log.debug("NotEnoughMoney <<<-$->>>");
            request.setAttribute(Request.NOT_ENOUGH_MONEY,
                    ErrorMassageFinder.getResourceBundle(request).getString("error_msg.not_enough_money"));
            return forward;
        }

        List<Edition> allInCart = (List<Edition>) session.getAttribute(Session.ALL_IN_CART);

        if (allInCart != null && allInCart.size() != 0) {
            List<Edition> tempToClearAllInCartList = new ArrayList<>();
            List<Edition> allOrders = (List<Edition>) session.getAttribute(Session.ALL_ORDERS);
            // It is recorded one order at a time, in case the connection with the database will broken.
            for (Edition edition : allInCart) {
                if (OrdersDAO.getInstance().createOrder(currentUser.getId(), edition.getId())) {
                    // change total cost to current
                    totalCost = RemoveFromCartCommand.findResidualTotalCost(totalCost, edition);
                    log.debug("totalCost === " + totalCost);
                    currentUser.setBalance(currentUser.getBalance() - edition.getPrice());
                    log.debug("currentUser balance == " + currentUser.getBalance());
                    UserDAO.getInstance().updateUserData(currentUser);
                    allOrders.add(edition);
                    tempToClearAllInCartList.add(edition);
                } else {
                    request.setAttribute(Request.SOMETHING_WRONG,
                            ErrorMassageFinder.getResourceBundle(request).getString("error_msg.server_db_access_error"));
                    clearAllInCart(allInCart, tempToClearAllInCartList);
                    return forward;
                }
            }
            clearAllInCart(allInCart, tempToClearAllInCartList);
            log.debug("allInCart size == " + allInCart.size());
            session.setAttribute(Session.CART_TOTAL_COST, totalCost);
            request.setAttribute(Request.ORDER_DONE, ErrorMassageFinder.getResourceBundle(request).getString("success_msg.subscription_successful"));
        }
        return forward;
    }

    private void clearAllInCart(List<Edition> allInCart, List<Edition> temp) {
        for (Edition edition : temp) {
            EditionFinder.deleteCurrentEditionFromList(edition, allInCart);
        }
    }

}