package com.periodicals.demo.web.command;

import com.periodicals.demo.web.command.admin_command.AddEdition;
import com.periodicals.demo.web.command.admin_command.AddGenre;
import com.periodicals.demo.web.command.admin_command.DeleteEdition;
import com.periodicals.demo.web.command.admin_command.EditEdition;
import com.periodicals.demo.web.command.navigation.admin.*;
import com.periodicals.demo.web.command.cart_command.AddToCartCommand;
import com.periodicals.demo.web.command.cart_command.RemoveFromCartCommand;
import com.periodicals.demo.web.command.cart_command.SubscribeAllCommand;
import com.periodicals.demo.web.command.cart_command.SubscribeFromCart;
import com.periodicals.demo.web.command.navigation.*;
import com.periodicals.demo.web.command.sort_find_comand.FindEdition;
import com.periodicals.demo.web.command.sort_find_comand.ListByGenre;
import com.periodicals.demo.web.command.sort_find_comand.SortBy;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {

    private static final Logger log = Logger.getLogger(CommandContainer.class);

    private static final Map<String, ICommand> COMMANDS = new TreeMap<String, ICommand>();

    static {
        COMMANDS.put("login", new LoginCommand());
        COMMANDS.put("logout", new LogoutCommand());
        COMMANDS.put("register", new RegisterCommand());
        COMMANDS.put("noCommand", new NoCommand());
        COMMANDS.put("changeLanguage", new ChangeLanguage());
        // navigation
        COMMANDS.put("replenishPage", new ReplenishPage());
        COMMANDS.put("settingsPage", new SettingsPage());
        COMMANDS.put("subscriptionsPage", new MySubscriptionsPage());
        COMMANDS.put("mainPage", new MainPage());
        COMMANDS.put("cartPage", new CartPage());

        COMMANDS.put("updateSettings", new UpdateSettingsCommand());

        // client commands
        COMMANDS.put("listByGenre", new ListByGenre());
        COMMANDS.put("find", new FindEdition());
        COMMANDS.put("sort", new SortBy());
        COMMANDS.put("subscribe", new SubscribeCommand());
        COMMANDS.put("unsubscribe", new UnsubscribeCommand());

        COMMANDS.put("addToCart", new AddToCartCommand());
        COMMANDS.put("removeFromCart", new RemoveFromCartCommand());
        COMMANDS.put("subscribeAll", new SubscribeAllCommand());
        COMMANDS.put("subscribeFromCart", new SubscribeFromCart());

        // admin commands
        COMMANDS.put("deleteEdition", new DeleteEdition());
        COMMANDS.put("listUsers", new ListUsers());
        COMMANDS.put("blockUser", new UserBlocker());
        COMMANDS.put("addEditionPage", new AddEditionPage());
        COMMANDS.put("addEdition", new AddEdition());

        COMMANDS.put("addGenrePage", new AddGenrePage());
        COMMANDS.put("addGenre", new AddGenre());
        COMMANDS.put("editEditionPage", new EditEditionPage());
        COMMANDS.put("editEdition", new EditEdition());

        log.debug("Command container was successfully initialized");
        log.trace("Number of commands --> " + COMMANDS.size());
    }

    public static ICommand getCommandHandler(String commandName) {
        if (commandName == null || !COMMANDS.containsKey(commandName)) {
            return COMMANDS.get("noCommand");
        }
        return COMMANDS.get(commandName);
    }

}
