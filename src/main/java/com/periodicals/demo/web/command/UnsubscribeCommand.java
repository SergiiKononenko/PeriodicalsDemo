package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.OrdersDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.EditionFinder;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class UnsubscribeCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_SUBSCRIPTIONS;
        String editionIdStr = request.getParameter("editionId");
        if (editionIdStr != null && !"".equals(editionIdStr)) {
            int editionIdInt = Integer.parseInt(editionIdStr);

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(Session.USER);

            List<Edition> ordersList = (List<Edition>) session.getAttribute(Session.ALL_ORDERS);

            Edition currentEdition = EditionFinder.getEditionFromListById(editionIdInt, ordersList);
            if (currentEdition != null) {
                OrdersDAO.getInstance().deleteOrder(user.getId(), editionIdInt);
                EditionFinder.deleteCurrentEditionFromList(currentEdition, ordersList);
                try {
                    response.sendRedirect(Path.COMMAND_MY_SUBSCRIPTIONS);
                    forward = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return forward;
    }

}
