package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public final class Pagination {

    private static final int QTY_EDITIONS_ON_PAGINATION_PAGES = 3;
    private static final String PAGINATION_FIRST_PAGES = "1";

    public static void setUpPagination(HttpServletRequest request, List<Edition> allEditions) {
        if (allEditions == null) {
            allEditions = new ArrayList<>();
        }

        HttpSession session = request.getSession();

        int[] pageQuantity = null;

        if (allEditions.size() > QTY_EDITIONS_ON_PAGINATION_PAGES) {
            // Determine page quantity and write it to request
            pageQuantity = determinePageQuantity(QTY_EDITIONS_ON_PAGINATION_PAGES, allEditions);
        }

        session.setAttribute(Request.PAGE_QUANTITY, pageQuantity);

        // Determine content for a concrete page.
        String pageNum = request.getParameter(Request.PAGE);
        if (pageNum == null || "".equals(pageNum)) {
            pageNum = PAGINATION_FIRST_PAGES;
        }
        List<Edition> currentPageContent = Pagination.determinePageToLoad(pageNum, QTY_EDITIONS_ON_PAGINATION_PAGES, allEditions);
        session.setAttribute(Session.PAGINATION_PAGE, pageNum);
        session.setAttribute(Session.FOUND_EDITIONS, currentPageContent);
    }

    public static List<Edition> determinePageToLoad(String pageNum, int qtyEditionsOnPaginationPages, List<Edition> allEditions) {
        int pageNumInt = Integer.parseInt(pageNum);
        int startIndex = (pageNumInt - 1) * qtyEditionsOnPaginationPages;
        int finishIndex = startIndex + qtyEditionsOnPaginationPages;

        List<Edition> onePageList = new ArrayList<>();
        for (int i = startIndex; i < finishIndex; i++) {
            if (i == allEditions.size()) {
                break;
            }
            onePageList.add(allEditions.get(i));
        }
        return onePageList;
    }

    public static int[] determinePageQuantity(int qtyOfPaginationPages, List<Edition> allEditions) {
        int pageQty = (int) Math.ceil((double) allEditions.size() / qtyOfPaginationPages);
        int[] paginationPagesNums = new int[pageQty];
        for (int i = 0; i < pageQty; i++) {
            paginationPagesNums[i] = i + 1;
        }
        return paginationPagesNums;
    }

}
