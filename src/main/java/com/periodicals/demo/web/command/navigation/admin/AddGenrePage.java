package com.periodicals.demo.web.command.navigation.admin;

import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddGenrePage implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return Path.PAGE_ADD_GENRE;
    }
}
