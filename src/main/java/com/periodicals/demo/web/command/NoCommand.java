package com.periodicals.demo.web.command;

import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.page_not_found");
        request.setAttribute("errorMessage", errorMessage);

        return Path.PAGE_ERROR_PAGE;
    }
}
