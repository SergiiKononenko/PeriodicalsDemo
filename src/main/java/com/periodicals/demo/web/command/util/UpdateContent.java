package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public final class UpdateContent {

    private static final String ENGLISH = "en";
    private static final String RUSSIAN = "ru";

    public static void updateAllSessionContent(HttpSession session, String language) {
        // Update current page content
        List<Edition> currentPageContent = (List<Edition>) session.getAttribute(Session.FOUND_EDITIONS);
        session.setAttribute(Session.FOUND_EDITIONS, updatedCurrentContentData(currentPageContent, language));

        // Update current session content
        List<Edition> currentSessionContent = (List<Edition>) session.getAttribute(Session.ALL_EDITIONS);
        session.setAttribute(Session.ALL_EDITIONS, updatedCurrentContentData(currentSessionContent, language));

        // Update all orders content
        List<Edition> allOrders = (List<Edition>) session.getAttribute(Session.ALL_ORDERS);
        session.setAttribute(Session.ALL_ORDERS, updatedCurrentContentData(allOrders, language));

        // Update all in cart content
        List<Edition> allInCart = (List<Edition>) session.getAttribute(Session.ALL_IN_CART);
        session.setAttribute(Session.ALL_IN_CART, updatedCurrentContentData(allInCart, language));
    }

    private static List<Edition> updatedCurrentContentData(List<Edition> currentContent, String language) {
        List<Edition> updatedContent = new ArrayList<>();

        if (currentContent != null) {
            for (Edition edition : currentContent) {
                updatedContent.add((EditionDAO.getInstance().findEditionsByName(edition.getEdName(), language, reverseLanguage(language))).get(0));
            }
        }
        return updatedContent;
    }

    private static String reverseLanguage(String language) {
        return (ENGLISH.equals(language)) ? RUSSIAN : ENGLISH;
    }


}
