package com.periodicals.demo.web.command;

import com.periodicals.demo.web.command.util.UpdateContent;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeLanguage implements ICommand {

    private static final Logger log = Logger.getLogger(ChangeLanguage.class);
    private static final String LANGUAGE_PARAM = "?lang=";


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String language = request.getParameter(Request.SET_LOCALE);
        log.debug("Language == " + language);

        session.setAttribute(Session.LANGUAGE, language);

        // Restart content to download actual data
        UpdateContent.updateAllSessionContent(session, language);

        return Path.PAGE_MAIN + LANGUAGE_PARAM + language;

    }

}
