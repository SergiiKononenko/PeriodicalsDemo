package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.EditionFinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class RemoveFromCartCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        List<Edition> allInCartList = (List<Edition>) session.getAttribute("allInCart");
        double cartTotalCost = (double) session.getAttribute("cartTotalCost");

        String editionIdStr = request.getParameter("editionId");
        int editionIdInt = Integer.parseInt(editionIdStr);

        Edition currentEdition = EditionFinder.getEditionFromListById(editionIdInt, allInCartList);

        double totalCost = 0;

        if (currentEdition != null) {
            EditionFinder.deleteCurrentEditionFromList(currentEdition, allInCartList);
            totalCost = findResidualTotalCost(cartTotalCost, currentEdition);
        }
        session.setAttribute("cartTotalCost", totalCost);

        return Path.COMMAND_CART_PAGE;
    }

    static double findResidualTotalCost(double cartTotalCost, Edition currentEdition) {
        double total = cartTotalCost - currentEdition.getPrice();
        return Math.round((total * 100.0) / 100.0);
    }
}
