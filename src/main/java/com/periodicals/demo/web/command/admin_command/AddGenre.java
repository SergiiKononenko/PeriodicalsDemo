package com.periodicals.demo.web.command.admin_command;

import com.periodicals.demo.db.GenreDAO;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.FieldsValidator;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddGenre implements ICommand {

    private static final Logger log = Logger.getLogger(AddGenre.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_ADD_GENRE;

        String newGenreNameEn = request.getParameter(Request.GENRE_NAME_EN);
        String newGenreNameRu = request.getParameter(Request.GENRE_NAME_RU);

        // Check validation
        if (!FieldsValidator.isGenreEnglishFieldValid(request, newGenreNameEn)) {
            return forward;
        } else if (!FieldsValidator.isGenreRussianFieldValid(request, newGenreNameRu)) {
            return forward;
        }

        // If OK - redirect to Add Edition
        if (GenreDAO.getInstance().createGenre(newGenreNameEn, newGenreNameRu)) {
            try {
                response.sendRedirect(Path.COMMAND_ADD_EDITION);
                forward = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return forward;
    }
}
