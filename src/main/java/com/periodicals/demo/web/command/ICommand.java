package com.periodicals.demo.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Main interface for the Command pattern implementation.
 * @author KSV
 */
public interface ICommand {
    String execute(HttpServletRequest request,
                   HttpServletResponse response);
}
