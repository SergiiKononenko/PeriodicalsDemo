package com.periodicals.demo.web.command.navigation.admin;

import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserBlocker implements ICommand {

    private static final Logger log = Logger.getLogger(UserBlocker.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.COMMAND_LIST_USERS;
        String userIdStr = request.getParameter(Request.USER_ID);
        if (userIdStr != null && !"".equals(userIdStr)) {
            int userIdInt = Integer.parseInt(userIdStr);
            // Check blocking current user
            boolean isBlocked = request.getParameter(Request.USER_IS_BLOCKED).equals("false");
            boolean isChanged = UserDAO.getInstance().setBlockingToUserById(userIdInt, isBlocked);
            if (isChanged) {
                try {
                    log.debug("NOW REDIRECT");
                    response.sendRedirect(Path.COMMAND_LIST_USERS);
                    forward = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return forward;
    }
}
