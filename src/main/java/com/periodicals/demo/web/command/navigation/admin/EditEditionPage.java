package com.periodicals.demo.web.command.navigation.admin;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.GenreDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.Genre;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class EditEditionPage implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String editionIdStr = request.getParameter(Request.EDITION_ID);
        if (editionIdStr != null && !"".equals(editionIdStr)) {
            int editionIdInt = Integer.parseInt(editionIdStr);
            HttpSession session = request.getSession();

            String language = (String) session.getAttribute(Session.LANGUAGE);

            // Get current editions from DB by id. First in list - en, second - ru
            List<Edition> editionsFromDB = EditionDAO.getInstance().findEditionsById(editionIdInt, language);
            Edition currentEditionEn = editionsFromDB.get(0);
            Edition currentEditionRu = editionsFromDB.get(1);

            request.setAttribute(Request.ADD_EDITION_CURRENT_EN, currentEditionEn);
            request.setAttribute(Request.ADD_EDITION_CURRENT_RU, currentEditionRu);

            Genre currentGenre = GenreDAO.getInstance().getGenreByName(currentEditionEn.getGenreName());
            request.setAttribute(Request.GENRE_CURRENT, currentGenre);
        }
        return Path.PAGE_EDIT_EDITION;
    }
}
