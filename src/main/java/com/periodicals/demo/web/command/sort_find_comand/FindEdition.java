package com.periodicals.demo.web.command.sort_find_comand;

import com.periodicals.demo.web.command.util.Pagination;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FindEdition implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String editionName = request.getParameter("editionName");

        if (editionName != null && !"".equals(editionName)) {
            HttpSession session = request.getSession();
            String language = (String) session.getAttribute(Session.LANGUAGE);
            List<Edition> allEditions = EditionDAO.getInstance().findEditionsByName(editionName, language);
            if (allEditions != null) {
                session.setAttribute(Session.ALL_EDITIONS, allEditions);
                Pagination.setUpPagination(request, allEditions);
            }
        }
        return Path.PAGE_MAIN;
    }

}