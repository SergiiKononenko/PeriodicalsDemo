package com.periodicals.demo.web.command.sort_find_comand;

import com.periodicals.demo.web.command.util.Pagination;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.Sorter;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SortBy implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String sortBy = request.getParameter("sortBy");

        List<Edition> editionList = (List<Edition>) request.getSession().getAttribute(Session.ALL_EDITIONS);

        Sorter.sortEditionListByValue(sortBy, editionList);

        // Set pagination and show 1-st page of pagination after sort
        Pagination.setUpPagination(request, editionList);

        return Path.PAGE_MAIN;
    }
}
