package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.EditionFinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.periodicals.demo.web.command.util.EditionFinder.getEditionFromListById;

public class AddToCartCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String editionIdStr = request.getParameter("editionId");

        if (editionIdStr != null && !"".equals(editionIdStr)) {
            int editionIdInt = Integer.parseInt(editionIdStr);

            HttpSession session = request.getSession();
            List<Edition> editionsList = (List<Edition>) session.getAttribute("foundEditions");

            Edition currentEdition = getEditionFromListById(editionIdInt, editionsList);
            if (currentEdition != null) {
                List<Edition> allInCart;
                // Check if allInCart not exists in session. If not than create it.
                if ((allInCart = (List<Edition>) session.getAttribute("allInCart")) == null) {
                    allInCart = new ArrayList<>();
                    session.setAttribute("allInCart", allInCart);
                }
                if (!allInCart.contains(currentEdition)) {
                    allInCart.add(currentEdition);
                    // Find total cost in cart
                    double cartTotalPrice = findTotalCostInCart(allInCart);
                    session.setAttribute("cartTotalCost", cartTotalPrice);
                    EditionFinder.deleteCurrentEditionFromList(currentEdition, editionsList);
                }
            }
        }
        return Path.PAGE_MAIN;
    }

    private double findTotalCostInCart(List<Edition> allInCart) {
        double total = 0;
        for (Edition edition :allInCart) {
            total += edition.getPrice();
        }
        return Math.round((total * 100.0) / 100.0);
    }

}
