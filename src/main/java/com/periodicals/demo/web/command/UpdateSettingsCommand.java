package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.FieldsValidator;
import com.periodicals.demo.web.command.util.PasswordEncryption;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UpdateSettingsCommand implements ICommand {

    private static final Logger log = Logger.getLogger(UpdateSettingsCommand.class);
    private String page;

    public String execute(HttpServletRequest request, HttpServletResponse response) {

        //get current user from session
        HttpSession session = request.getSession();
        User fromSessionUser = (User) session.getAttribute(Session.USER);

        log.debug("FromSessionUser: " + fromSessionUser);

        //create temp user to update DB
        User tempUser = new User();
        //id and login unmodified field
        tempUser.setLogin(fromSessionUser.getLogin());
        tempUser.setId(fromSessionUser.getId());

        findOutChangedUserFields(request, fromSessionUser, tempUser);
        if (request.getAttribute("invalidFieldMessage") != null) {
            return page;
        }

        log.debug("tempUser ===== " + tempUser);

        if (UserDAO.getInstance().updateUserData(tempUser)) {
            session.setAttribute(Session.USER, tempUser);
        }

        try {
            response.sendRedirect(request.getParameter("currentPage"));
            log.debug("Redirect -> " + request.getContextPath() + "controller?command=replenishPage");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return page;
    }

    private void findOutChangedUserFields(HttpServletRequest request, User fromSessionUser, User tempUser) {
        String pass = request.getParameter("password");
        String balance = request.getParameter("amount");
        String isBlocked = request.getParameter("blocking");
        String role = request.getParameter("role");

        log.debug("Fields to change: " + pass + " " + balance + " " + isBlocked + " " + role);

        if (pass != null) {
            if (!FieldsValidator.isPasswordFieldValid(request, pass)) {
                page = Path.PAGE_SETTINGS;
                return;
            }
            pass = PasswordEncryption.md5String(pass);
            tempUser.setPassword(pass);
        } else {
            tempUser.setPassword(fromSessionUser.getPassword());
        }

        if (balance != null) {
            if (!FieldsValidator.isPaymentFieldValid(request, balance)) {
                page = Path.PAGE_REPLENISH;
                return;
            }
            tempUser.setBalance(Double.parseDouble(balance) + fromSessionUser.getBalance());
        } else {
            tempUser.setBalance(fromSessionUser.getBalance());
        }

        if (isBlocked != null) {
            tempUser.setBlocked(Boolean.parseBoolean(isBlocked));
        } else {
            tempUser.setBlocked(fromSessionUser.isBlocked());
        }

        if (role != null) {
            tempUser.setUserRole(Integer.parseInt(role));
        } else {
            tempUser.setUserRole(fromSessionUser.getUserRole());
        }
    }

}
