package com.periodicals.demo.web.command.util;

import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

public final class ErrorMassageFinder {

    public static ResourceBundle getResourceBundle(HttpServletRequest request) {
        String language = (String) request.getSession().getAttribute(Session.LANGUAGE);
        if (language == null) {
            language = "";
        }
        return ResourceBundle.getBundle("resources", new Locale(language));
    }

}
