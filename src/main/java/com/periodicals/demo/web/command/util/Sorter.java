package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;

import java.util.Comparator;
import java.util.List;

public final class Sorter {

    public static void sortEditionListByValue(String value, List<Edition> list) {
        if (list != null) {
            if ("name".equals(value)) {
                list.sort(Comparator.comparing(Edition::getEdName));
            } else if ("priceUp".equals(value)) {
                list.sort(Comparator.comparing(Edition::getPrice));
            } else if ("priceDown".equals(value)) {
                list.sort(Comparator.comparing(Edition::getPrice).reversed());
            }
        }
    }

}
