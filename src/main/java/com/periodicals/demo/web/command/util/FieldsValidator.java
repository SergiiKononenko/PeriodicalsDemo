package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.GenreDAO;
import com.periodicals.demo.web.constant.Request;

import javax.servlet.http.HttpServletRequest;

public final class FieldsValidator {

    private static final String ENGLISH = "en";
    private static final String RUSSIAN = "ru";

    public static boolean isLoginPageFieldsValid(HttpServletRequest request, String login, String password) {
        String errorMessage;
        if (login == null || login.equals("")) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_login");
            request.setAttribute("errorMessageLog", errorMessage);
            return false;
        } else if (password == null || password.equals("")) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_password");
            request.setAttribute("errorMessagePass", errorMessage);
            return false;
        } else if (!isPasswordFieldValid(request, password)) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_password_field");
            request.setAttribute("errorMessagePass", errorMessage);
            return false;
        }
        return true;
    }

    public static boolean isPaymentFieldValid(HttpServletRequest request, String balance) {
        String errorMessage;
        if ("".equals(balance) || !balance.matches("^[0-9]*[.]?[0-9]{0,2}$") || Double.parseDouble(balance) < 1) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.replenish_invalid");

            request.setAttribute("invalidFieldMessage", errorMessage);
            return false;
        }
        return true;
    }

    public static boolean isPasswordFieldValid(HttpServletRequest request, String password) {
        String errorMessage;
        if ("".equals(password) || !password.matches(".{3,10}")) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.invalid_password_field");
            request.setAttribute("invalidFieldMessage", errorMessage);
            return false;
        }
        return true;
    }

    public static boolean isGenreEnglishFieldValid(HttpServletRequest request, String genreName) {
        String errorMessage;
        // Check if field is empty
        if (genreName == null || "".equals(genreName)) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_field");
            request.setAttribute(Request.ADD_GENRE_EM_EN, errorMessage);
            return false;
        }
        // Check if genre matches
        if (!genreName.matches("[a-zA-Z]{3,10}")) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("genre.english_only");
            request.setAttribute(Request.ADD_GENRE_EM_EN, errorMessage);
            return false;
        }
        // Check if genre exists
        if (GenreDAO.getInstance().isExistsGenreName(genreName, ENGLISH)) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.unique_genre");
            request.setAttribute(Request.ADD_GENRE_EM_EN, errorMessage);
            return false;
        }

        return true;
    }

    public static boolean isGenreRussianFieldValid(HttpServletRequest request, String genreName) {
        String errorMessage;
        // Check if field is empty
        if (genreName == null || "".equals(genreName)) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.empty_field");
            request.setAttribute(Request.ADD_GENRE_EM_RU, errorMessage);
            return false;
        }
        // Check if genre matches
        if (!genreName.matches("[а-яА-яёЁэ]{3,10}")) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("genre.russian_only");
            request.setAttribute(Request.ADD_GENRE_EM_RU, errorMessage);
            return false;
        }
        // Check if genre exists
        else if (GenreDAO.getInstance().isExistsGenreName(genreName, RUSSIAN)) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.unique_genre");
            request.setAttribute(Request.ADD_GENRE_EM_RU, errorMessage);
            return false;
        }

        return true;
    }

}
