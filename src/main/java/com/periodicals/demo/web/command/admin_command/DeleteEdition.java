package com.periodicals.demo.web.command.admin_command;

import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.util.EditionFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class DeleteEdition implements ICommand {

    private static final Logger log = Logger.getLogger(DeleteEdition.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_MAIN;
        String editionIdStr = request.getParameter(Request.EDITION_ID);
        int editionIdInt;
        if (editionIdStr != null && !"".equals(editionIdStr)) {
            editionIdInt = Integer.parseInt(editionIdStr);

            // Find current edition in session by ID.
            // Finish method if not exists.
            HttpSession session = request.getSession();
            List<Edition> allEditions = (List<Edition>) session.getAttribute(Session.FOUND_EDITIONS);

            Edition currentEdition = EditionFinder.getEditionFromListById(editionIdInt, allEditions);
            if (currentEdition == null) {
                return forward;
            }

            // Else
            boolean isDeleted = EditionDAO.getInstance().deleteEdition(currentEdition);
            if (isDeleted) {
                EditionFinder.deleteCurrentEditionFromList(currentEdition, allEditions);
            }
        }

        return forward;
    }
}
