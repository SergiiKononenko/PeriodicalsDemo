package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;

import java.util.List;

public final class EditionFinder {

    public static Edition getEditionFromListById(int editionIdInt, List<Edition> editionList) {
        Edition edition = null;
        for (Edition e : editionList) {
            if (e.getId() == editionIdInt) {
                edition = e;
            }
        }
        return edition;
    }

    public static void deleteCurrentEditionFromList(Edition currentEdition, List<Edition> ordersList) {
        ordersList.removeIf(edition -> edition.getId() == currentEdition.getId());
    }

}
