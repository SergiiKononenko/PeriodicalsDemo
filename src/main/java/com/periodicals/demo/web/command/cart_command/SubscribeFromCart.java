package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.db.OrdersDAO;
import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.command.SubscribeCommand;
import com.periodicals.demo.web.command.util.EditionFinder;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SubscribeFromCart implements ICommand {

    private static final Logger log = Logger.getLogger(SubscribeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String editionIdStr = request.getParameter(Request.EDITION_ID);
        String forward = Path.PAGE_CART;

        int editionIdInt;
        if (editionIdStr != null && !"".equals(editionIdStr)) {
            editionIdInt = Integer.parseInt(editionIdStr);
            List<Edition> allInCart = (List<Edition>) session.getAttribute(Session.ALL_IN_CART);

            Edition currentEdition = EditionFinder.getEditionFromListById(editionIdInt, allInCart);
            if (currentEdition != null) {
                User user = (User) session.getAttribute(Session.USER);

                // check have enough money to subscribe
                if (user.getBalance() - currentEdition.getPrice() < 0) {
                    request.setAttribute(Request.NOT_ENOUGH_MONEY,
                            ErrorMassageFinder.getResourceBundle(request).getString("error_msg.not_enough_money"));
                    return forward;
                }
                if (OrdersDAO.getInstance().createOrder(user.getId(), currentEdition.getId())) {
                    // change total cost to current
                    double totalCost = (double) session.getAttribute(Session.CART_TOTAL_COST);
                    // Set new value of total cost in the cart
                    totalCost = RemoveFromCartCommand.findResidualTotalCost(totalCost, currentEdition);
                    session.setAttribute(Session.CART_TOTAL_COST, totalCost);
                    log.debug("totalCost === " + totalCost);

                    user.setBalance(user.getBalance() - currentEdition.getPrice());
                    log.debug("currentUser balance == " + user.getBalance());
                    UserDAO.getInstance().updateUserData(user);

                    List<Edition> allOrders = (List<Edition>) session.getAttribute(Session.ALL_ORDERS);
                    allOrders.add(currentEdition);
                    // Clean cart from ordered edition
                    EditionFinder.deleteCurrentEditionFromList(currentEdition, allInCart);
                }
            }

        }

        return forward;
    }
}
