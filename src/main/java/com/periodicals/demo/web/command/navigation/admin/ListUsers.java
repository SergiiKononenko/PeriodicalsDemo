package com.periodicals.demo.web.command.navigation.admin;

import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListUsers implements ICommand {

    private static final Logger log = Logger.getLogger(UserBlocker.class);

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardPage = Path.PAGE_USERS;

        // Get content for Users-page
        List<User> allUsers = UserDAO.getInstance().getAllUsers();
        request.setAttribute(Request.ALL_USERS, allUsers);
        log.debug("NOW FORWARD");

        return forwardPage;
    }
}
