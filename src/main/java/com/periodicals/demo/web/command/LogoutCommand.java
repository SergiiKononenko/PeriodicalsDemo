package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements ICommand {

    private static final Logger log = Logger.getLogger(LogoutCommand.class);

    public String execute(HttpServletRequest request, HttpServletResponse response) {

        log.debug("Logout command starts");

        HttpSession session = request.getSession();
        if (session != null) {
            session.setAttribute(Session.USER, null);
        }
        log.debug("Logout command complete");

        return Path.PAGE_MAIN;
    }

}
