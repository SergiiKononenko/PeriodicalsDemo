package com.periodicals.demo.web.command;

import com.periodicals.demo.db.OrdersDAO;
import com.periodicals.demo.db.UserDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.command.util.FieldsValidator;
import com.periodicals.demo.web.command.util.PasswordEncryption;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class LoginCommand implements ICommand {

    private static final Logger log = Logger.getLogger(LoginCommand.class);

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        // Obtain login and password from the request
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        log.trace("Request parameter: login --> " + login);

        // Error handler
        String errorMessage;
        String forward = Path.PAGE_LOGIN;

        if (!FieldsValidator.isLoginPageFieldsValid(request, login, password)) {
            return forward;
        }

        //Look up user in the database
        User user = UserDAO.getInstance().findUserByLogin(login);

        password = PasswordEncryption.md5String(password);

        if (user == null || !password.equals(user.getPassword())) {
            // Error massage depends of language:
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.login.user_not_found");

            request.setAttribute("userNotFound", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        } else if (user.isBlocked()) {
            errorMessage = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.login.account_blocked");
            request.setAttribute(Request.ERROR_MSG, errorMessage);
            forward = Path.PAGE_ERROR_PAGE;
        } else {
            session.setAttribute(Session.USER, user);
            String language = (String) session.getAttribute(Session.LANGUAGE);
            List<Edition> list = OrdersDAO.getInstance().getAllOrders(user.getId(), language);
            log.debug("List<Edition> OrdersDAO.getInstance().getAllOrders() -> " + list);

            session.setAttribute(Session.ALL_ORDERS, list);
            forward = Path.PAGE_MAIN;
        }
        return forward;
    }

}
