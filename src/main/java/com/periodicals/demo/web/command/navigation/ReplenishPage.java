package com.periodicals.demo.web.command.navigation;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReplenishPage implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return Path.PAGE_REPLENISH;
    }
}
