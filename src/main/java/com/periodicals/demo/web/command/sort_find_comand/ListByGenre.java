package com.periodicals.demo.web.command.sort_find_comand;

import com.periodicals.demo.web.command.util.Pagination;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.db.EditionDAO;
import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.command.ICommand;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class ListByGenre implements ICommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String genreName = request.getParameter("genreName");
        String forward = Path.PAGE_MAIN;

        if (genreName != null && !"".equals(genreName)) {
            HttpSession session = request.getSession();
            String language = (String) session.getAttribute(Session.LANGUAGE);
            List<Edition> allEditions = EditionDAO.getInstance().getAllEditions(language);

            if (genreName.equals("all")) {
                session.setAttribute(Session.ALL_EDITIONS, allEditions);
                Pagination.setUpPagination(request, allEditions);
                return forward;
            }

            List<Edition> concreteGenreEditions = null;

            for (Edition edition : allEditions) {
                if (edition.getGenreName().equals(genreName)) {
                    if (concreteGenreEditions == null) {
                        concreteGenreEditions = new ArrayList<>();
                    }
                    concreteGenreEditions.add(edition);
                }
            }
            session.setAttribute(Session.ALL_EDITIONS, concreteGenreEditions);
            Pagination.setUpPagination(request, concreteGenreEditions);
        }

        return forward;
    }
}
