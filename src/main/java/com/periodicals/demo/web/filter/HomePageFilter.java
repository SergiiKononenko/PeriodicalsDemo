package com.periodicals.demo.web.filter;

import com.periodicals.demo.web.constant.Path;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(Path.PAGE_MAIN)
public class HomePageFilter implements Filter {

    private static final Logger log = Logger.getLogger(HomePageFilter.class);

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        httpResponse.sendRedirect(Path.COMMAND_MAIN_PAGE);

        log.debug("Redirect after filter");

        // It is forbidden to put chain.doFilter here, otherwise an endless loop.
    }

    public void destroy() {
    }
}
