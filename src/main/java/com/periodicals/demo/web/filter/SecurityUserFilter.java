package com.periodicals.demo.web.filter;

import com.periodicals.demo.db.entity.User;
import com.periodicals.demo.web.command.util.ErrorMassageFinder;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebFilter(servletNames = "Controller", urlPatterns = "/controller")
public class SecurityUserFilter implements Filter {

    private static final int ROLE_USER = 2;
    private static final Set<String> userCommands = new HashSet<String>() {{
        // Add all admin commands
        add("replenishPage");
        add("settingsPage");
        add("subscriptionsPage");
        add("cartPage");
    }};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String errorMsg = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.user_access_only");
        checkAccess(servletRequest, servletResponse, filterChain, userCommands, ROLE_USER, errorMsg);
    }

    @Override
    public void destroy() {
    }

    static void checkAccess(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, Set<String> userCommands, int roleUser, String errorMsg) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        User user = (User) request.getSession().getAttribute(Session.USER);

        String currentCommand = request.getParameter("command");

        // Unregistered user
        if (user == null && userCommands.contains(currentCommand)) {
            accessDeniedSendToErrorPage(servletRequest, servletResponse, errorMsg);
        } else if ((user != null)
                && (user.getUserRole() != roleUser)
                && (userCommands.contains(currentCommand))) {
            accessDeniedSendToErrorPage(servletRequest, servletResponse, errorMsg);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private static void accessDeniedSendToErrorPage(ServletRequest servletRequest, ServletResponse servletResponse, String errorMsg) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        request.setAttribute(Request.ERROR_MSG, errorMsg);
        request.getRequestDispatcher(Path.PAGE_ERROR_PAGE).forward(servletRequest, servletResponse);
    }

}
