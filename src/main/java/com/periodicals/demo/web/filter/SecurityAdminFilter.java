package com.periodicals.demo.web.filter;

import com.periodicals.demo.web.command.util.ErrorMassageFinder;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebFilter(servletNames = "Controller", urlPatterns = "/controller")
public class SecurityAdminFilter implements Filter {

    private static final int ROLE_ADMIN = 1;
    private static final Set<String> adminCommands = new HashSet<String>() {{
        // Add all admin commands
        add("listUsers");
        add("addEditionPage");
        add("addGenrePage");
    }};

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String errorMsg = ErrorMassageFinder.getResourceBundle(request).getString("error_msg.admin_access_only");
        SecurityUserFilter.checkAccess(servletRequest, servletResponse, filterChain, adminCommands, ROLE_ADMIN, errorMsg);
    }

    @Override
    public void destroy() {
    }

}
