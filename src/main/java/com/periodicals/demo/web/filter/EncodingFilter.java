package com.periodicals.demo.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class EncodingFilter implements Filter {

	private static final Logger log = Logger.getLogger(EncodingFilter.class);
	private static final String ENCODING = "UTF-8";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.debug("ENCODING init");
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		request.setCharacterEncoding(ENCODING);
		response.setCharacterEncoding(ENCODING);
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void destroy() {
		log.debug("ENCODING destroy");
	}
}