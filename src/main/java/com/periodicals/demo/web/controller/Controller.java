package com.periodicals.demo.web.controller;

import com.periodicals.demo.web.command.CommandContainer;
import com.periodicals.demo.web.command.ICommand;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet controller.
 *
 * @author KSV
 */

@WebServlet("/controller")
public class Controller extends HttpServlet {

    private static final Logger log = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    /**
     * Main method of this controller.
     */
    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("Controller starts");
        String commandName = req.getParameter("command");
        log.trace("Request parameter: command --> " + commandName);

        ICommand command = CommandContainer.getCommandHandler(commandName);
        log.trace("Forward address --> " + commandName);

        String forward = command.execute(req, resp);
        log.trace("Forward address --> " + forward);

        log.debug("Controller finished, now go to forward address without redirect --> " + forward);

        if (forward != null) {
            RequestDispatcher dispatcher = req.getRequestDispatcher(forward);
            dispatcher.forward(req, resp);
        }
    }

}
