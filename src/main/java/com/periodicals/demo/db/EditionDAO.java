package com.periodicals.demo.db;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.util.DefineLanguage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EditionDAO {

    private final static String SQL_AS_GENRE_NAME_FIELD = "genreName";

    private static EditionDAO editionDAO;

    private EditionDAO() {
    }

    public static synchronized EditionDAO getInstance() {
        if (editionDAO == null) {
            editionDAO = new EditionDAO();
        }
        return editionDAO;
    }

    public List<Edition> getAllEditions(String language) {
        String lang = DefineLanguage.getLanguage(language);
        List<Edition> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT id, ed_name_" + lang + ", title_" + lang + ", price, pub_date, "
                    + "(SELECT " + DBFields.GENRE_NAME + lang + " FROM genres WHERE editions.genre_id = genres.id) AS "
                    + SQL_AS_GENRE_NAME_FIELD + " FROM editions");
            getAllEditions(list, rs, lang);
            statement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return list;
    }

    void getAllEditions(List<Edition> list, ResultSet rs, String lang) throws SQLException {
        while (rs.next()) {
            int id = rs.getInt(DBFields.ID);
            String edName = rs.getString(DBFields.EDITION_NAME + lang);
            String title = rs.getString(DBFields.EDITION_TITLE + lang);
            double price = rs.getDouble(DBFields.EDITION_PRICE);
            Date pubDate = rs.getDate(DBFields.EDITION_DATE);
            String genreName = rs.getString(SQL_AS_GENRE_NAME_FIELD);
            list.add(new Edition(id, edName, title, price, pubDate, genreName));
        }
        rs.close();
    }

    public List<Edition> findEditionsByName(String editionName, String language) {
        return findEditionsByName(editionName, language, language);
    }

    public List<Edition> findEditionsByName(String editionName, String language1, String language2) {
        String lang = DefineLanguage.getLanguage(language1);
        String lang2 = DefineLanguage.getLanguage(language2);
        Edition edition;
        List<Edition> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement prStatement;
        try {
            connection = DBManager.getInstance().getConnection();

            prStatement = connection.prepareStatement("SELECT id, ed_name_" + lang + ", title_" + lang + ", price, pub_date, "
                    + "(SELECT " + DBFields.GENRE_NAME + lang + " FROM genres WHERE editions.genre_id = genres.id) AS "
                    + SQL_AS_GENRE_NAME_FIELD + " FROM editions" + " WHERE " + DBFields.EDITION_NAME + lang2 + " LIKE ?");
            prStatement.setString(1, "%" + editionName + "%");

            ResultSet rs = prStatement.executeQuery();
            while (rs.next()) {
                edition = new Edition();
                edition.setId(rs.getInt(DBFields.ID));
                edition.setEdName(rs.getString(DBFields.EDITION_NAME + lang));
                edition.setTitle(rs.getString(DBFields.EDITION_TITLE + lang));
                edition.setPrice(rs.getDouble(DBFields.EDITION_PRICE));
                edition.setPubDate(rs.getDate(DBFields.EDITION_DATE));
                edition.setGenreName(rs.getString(SQL_AS_GENRE_NAME_FIELD));
                list.add(edition);
            }
            rs.close();
            prStatement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return list;
    }

    public List<Edition> findEditionsById(int editionId, String language) {
        String lang = DefineLanguage.getLanguage(language);
        List<Edition> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement prStatement;
        try {
            connection = DBManager.getInstance().getConnection();

            prStatement = connection.prepareStatement(
                    "SELECT *, (SELECT genre_" + lang + " FROM genres WHERE editions.genre_id = genres.id) AS "
                            + SQL_AS_GENRE_NAME_FIELD + " FROM editions WHERE id = ?");
            prStatement.setInt(1, editionId);

            ResultSet rs = prStatement.executeQuery();
            Edition editionEn;
            Edition editionRu;

            while (rs.next()) {
                editionEn = new Edition();
                editionRu = new Edition();

                editionEn.setId(rs.getInt(DBFields.ID));
                editionEn.setEdName(rs.getString(DBFields.EDITION_NAME + "en"));
                editionEn.setTitle(rs.getString(DBFields.EDITION_TITLE + "en"));
                editionEn.setPrice(rs.getDouble(DBFields.EDITION_PRICE));
                editionEn.setPubDate(rs.getDate(DBFields.EDITION_DATE));
                editionEn.setGenreName(rs.getString(SQL_AS_GENRE_NAME_FIELD));

                editionRu.setEdName(rs.getString(DBFields.EDITION_NAME + "ru"));
                editionRu.setTitle(rs.getString(DBFields.EDITION_TITLE + "ru"));

                list.add(editionEn);
                list.add(editionRu);
            }
            rs.close();
            prStatement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return list;
    }

    public boolean createEdition(List<Edition> newEditionsList) {
        Connection connection = null;
        boolean isCreated = false;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            PreparedStatement pStatement = connection.prepareStatement(
                    "INSERT INTO editions (ed_name_en, ed_name_ru, title_en, title_ru, price, pub_date, genre_id) "
                            + "values (?, ?, ?, ?, ?, ?, ?)");
            // Get editions from list of 2 edition (en and ru)
            Edition editionEnglish = newEditionsList.get(0);
            Edition editionRussian = newEditionsList.get(1);

            pStatement.setString(1, editionEnglish.getEdName());
            pStatement.setString(2, editionRussian.getEdName());
            pStatement.setString(3, editionEnglish.getTitle());
            pStatement.setString(4, editionRussian.getTitle());
            pStatement.setDouble(5, editionEnglish.getPrice());
            pStatement.setDate(6, new java.sql.Date(editionEnglish.getPubDate().getTime()));
            pStatement.setInt(7,
                    (GenreDAO.getInstance().getGenreByName(editionEnglish.getGenreName())).getId());

            pStatement.executeUpdate();
            pStatement.close();
            isCreated = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isCreated;
    }

    public boolean updateEdition(List<Edition> newEditionsList) {
        boolean isUpdate = false;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            // Get editions from list of 2 edition (en and ru)
            Edition editionEnglish = newEditionsList.get(0);
            Edition editionRussian = newEditionsList.get(1);

            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE editions SET "
                            + DBFields.EDITION_NAME + "en" + " = ?, "
                            + DBFields.EDITION_NAME + "ru" + " = ?, "
                            + DBFields.EDITION_TITLE + "en" + " = ?, "
                            + DBFields.EDITION_TITLE + "ru" + " = ?, "
                            + DBFields.EDITION_PRICE + " = ?, "
                            + DBFields.EDITION_DATE + " = ?, "
                            + DBFields.EDITION_GENRE_ID + " = ? "
                            + "WHERE " + DBFields.ID + " = ?"
            );
            int k = 1;
            pstmt.setString(k++, editionEnglish.getEdName());
            pstmt.setString(k++, editionRussian.getEdName());
            pstmt.setString(k++, editionEnglish.getTitle());
            pstmt.setString(k++, editionRussian.getTitle());
            pstmt.setDouble(k++, editionEnglish.getPrice());
            pstmt.setDate(k++, new java.sql.Date(editionEnglish.getPubDate().getTime()));
            pstmt.setInt(k++, (GenreDAO.getInstance().getGenreByName(editionEnglish.getGenreName())).getId());
            pstmt.setInt(k, editionEnglish.getId());
            pstmt.executeUpdate();

            pstmt.close();
            isUpdate = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isUpdate;
    }

    public boolean deleteEdition(Edition edition) {
        boolean isDeleted = false;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            PreparedStatement pstmt = connection.prepareStatement("DELETE FROM editions WHERE id = ?");
            pstmt.setInt(1, edition.getId());

            pstmt.executeUpdate();
            pstmt.close();
            isDeleted = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isDeleted;
    }

}
