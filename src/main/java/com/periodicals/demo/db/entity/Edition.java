package com.periodicals.demo.db.entity;

import java.util.Date;
import java.util.Objects;

public class Edition {

    private int id;
    private String edName;
    private String title;
    private double price;
    private Date pubDate;
    private String genreName;

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Edition(int id, String edName, String title, double price, Date pubDate, String genreName) {
        this.id = id;
        this.edName = edName;
        this.title = title;
        this.price = price;
        this.pubDate = pubDate;
        this.genreName = genreName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEdName() {
        return edName;
    }

    public void setEdName(String edName) {
        this.edName = edName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public Edition() {
    }

    public String getGenreName() {
        return genreName;
    }

    @Override
    public String toString() {
        return "Edition{" +
                "id=" + id +
                ", edName='" + edName + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", pubDate=" + pubDate +
                ", genreName=" + genreName +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edition edition = (Edition) o;
        return Objects.equals(edName, edition.edName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edName);
    }
}
