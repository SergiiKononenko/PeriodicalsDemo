package com.periodicals.demo.db.entity;

public class User {
    private int id;
    private String login;
    private String password;
    private double balance;
    private boolean isBlocked;
    private int userRole;

    public User() {
    }

    public User(int id, String login, String password, double balance, boolean isBlocked, int userRole) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.balance = balance;
        this.isBlocked = isBlocked;
        this.userRole = userRole;
    }

    public User(String login, String pass) {
        this.login = login;
        this.password = pass;
        this.balance = 0;
        this.isBlocked = false;
        this.userRole = 2;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", balance=" + balance +
                ", isBlocked=" + isBlocked +
                ", userRole=" + userRole +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

}
