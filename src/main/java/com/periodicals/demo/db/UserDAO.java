package com.periodicals.demo.db;

import com.periodicals.demo.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static UserDAO userDAO;

    private UserDAO() {
    }

    public static synchronized UserDAO getInstance() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }

    public List<User> getAllUsers() {
        List<User> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                int id = rs.getInt(DBFields.ID);
                String login = rs.getString(DBFields.USER_LOGIN);
                String pass = rs.getString(DBFields.USER_PASSWORD);
                double balance = rs.getDouble(DBFields.USER_BALANCE);
                boolean isBlocked = rs.getBoolean(DBFields.USER_IS_BLOCKED);
                int role = rs.getInt(DBFields.USER_ROLE);
                list.add(new User(id, login, pass, balance, isBlocked, role));
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }

        return list;
    }

    public boolean createUser(User user) {
        Connection connection = null;
        boolean isCreated = false;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);
            insertUser(connection, user);
            isCreated = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isCreated;
    }

    public User findUserByLogin(String login) {
        User user = null;
        Connection connection = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            connection = DBManager.getInstance().getConnection();

            pstmt = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                user = initLoggedUser(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return user;
    }

    public boolean updateUserData(User user) {
        boolean isUpdate = false;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);
            updateUser(connection, user);
            isUpdate = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isUpdate;
    }


    //Entity access methods (for transactions)

    private void insertUser(Connection connection, User user) throws SQLException {
        PreparedStatement pStatement = connection.prepareStatement(
                "INSERT INTO users (login, pass, balance, is_blocked, user_role) "
                        + "values (?, ?, ?, ?, ?)");
        pStatement.setString(1, user.getLogin());
        pStatement.setString(2, user.getPassword());
        pStatement.setDouble(3, user.getBalance());
        pStatement.setBoolean(4, user.isBlocked());
        pStatement.setInt(5, user.getUserRole());

        pStatement.executeUpdate();
        pStatement.close();
    }

    private void updateUser(Connection connection, User user) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(
                "UPDATE users SET "
                        + DBFields.USER_PASSWORD + " = ?,"
                        + DBFields.USER_BALANCE + " = ?,"
                        + DBFields.USER_IS_BLOCKED + " = ?,"
                        + DBFields.USER_ROLE + " = ? WHERE "
                        + DBFields.USER_LOGIN + " = ?"
        );
        int k = 1;
        pstmt.setString(k++, user.getPassword());
        pstmt.setDouble(k++, user.getBalance());
        pstmt.setBoolean(k++, user.isBlocked());
        pstmt.setInt(k++, user.getUserRole());
        pstmt.setString(k, user.getLogin());

        pstmt.executeUpdate();
        pstmt.close();
    }

    private User initLoggedUser(ResultSet rs) {
        User user = null;
        try {
            user = new User();
            user.setId(rs.getInt(DBFields.ID));
            user.setLogin(rs.getString(DBFields.USER_LOGIN));
            user.setPassword(rs.getString(DBFields.USER_PASSWORD));
            user.setBalance(rs.getDouble(DBFields.USER_BALANCE));
            user.setBlocked(rs.getBoolean(DBFields.USER_IS_BLOCKED));
            user.setUserRole(rs.getInt(DBFields.USER_ROLE));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }


    public boolean setBlockingToUserById(int userId, boolean isBlocked) {
        boolean isChanged = false;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            PreparedStatement pstmt = connection.prepareStatement(
                    "UPDATE users SET " + DBFields.USER_IS_BLOCKED + " = ? WHERE " + DBFields.ID + " = ?");
            pstmt.setBoolean(1, isBlocked);
            pstmt.setInt(2, userId);

            pstmt.executeUpdate();
            pstmt.close();
            isChanged = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }

        return isChanged;
    }
}
