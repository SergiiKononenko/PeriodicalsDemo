package com.periodicals.demo.db.util;

public final class DefineLanguage {

    private static final String DEFAULT_LANGUAGE = "en";

    public static String getLanguage(String sessionAttributeLanguage) {
        String language;
        if (sessionAttributeLanguage == null) {
            language = DEFAULT_LANGUAGE;
        } else {
            language = sessionAttributeLanguage;
        }
        return language;
    }

}
