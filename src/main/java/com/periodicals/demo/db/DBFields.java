package com.periodicals.demo.db;

public final class DBFields {

    public static final String ID = "id";
    //user
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "pass";
    public static final String USER_BALANCE = "balance";
    public static final String USER_IS_BLOCKED = "is_blocked";
    public static final String USER_ROLE = "user_role";

    //genres
    public static final String GENRE_NAME = "genre_";

    //editions
    public static final String EDITION_NAME = "ed_name_";
    public static final String EDITION_TITLE = "title_";
    public static final String EDITION_PRICE = "price";
    public static final String EDITION_DATE = "pub_date";
    public static final String EDITION_GENRE_ID = "genre_id";

    //orders
    public static final String ORDERS_USER_ID = "user_id";
    public static final String ORDERS_EDITION_ID = "edition_id";

}
