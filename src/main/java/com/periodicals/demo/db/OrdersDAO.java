package com.periodicals.demo.db;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.db.util.DefineLanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrdersDAO {

    private final static String SQL_AS_GENRE_NAME_FIELD = "genreName";

    private static OrdersDAO ordersDAO;

    private OrdersDAO() {
    }

    public static synchronized OrdersDAO getInstance() {
        if (ordersDAO == null) {
            ordersDAO = new OrdersDAO();
        }
        return ordersDAO;
    }

    public boolean createOrder(int userId, int editionId) {
        Connection connection = null;
        boolean isCreated = false;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            if (isAlreadyDone(connection, userId, editionId)) {
                return false;
            }

            PreparedStatement ps = connection.prepareStatement("INSERT INTO orders (" +
                    DBFields.ORDERS_USER_ID + ", " +
                    DBFields.ORDERS_EDITION_ID + ") VALUES (?, ?)"
            );
            ps.setInt(1, userId);
            ps.setInt(2, editionId);

            ps.executeUpdate();
            ps.close();

            isCreated = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isCreated;
    }

    private boolean isAlreadyDone(Connection connection, int userId, int editionId) {
        boolean isAlreadyDone = false;
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM orders WHERE "
                    + DBFields.ORDERS_USER_ID + " = ? AND "
                    + DBFields.ORDERS_EDITION_ID + " = ?"
            );
            ps.setInt(1, userId);
            ps.setInt(2, editionId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                isAlreadyDone = true;
            }
            ps.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return isAlreadyDone;
    }

    public List<Edition> getAllOrders(int userId, String language) {
        String lang = DefineLanguage.getLanguage(language);
        List<Edition> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement pStatement = connection.prepareStatement("SELECT id, ed_name_" + lang + ", title_" + lang + ", price, pub_date, " +
                    "(SELECT genre_" + lang + " FROM genres WHERE editions.genre_id = genres.id) AS " + SQL_AS_GENRE_NAME_FIELD +
                    " FROM editions join orders where orders.edition_id = editions.id and user_id = ?");
            pStatement.setInt(1, userId);
            ResultSet rs = pStatement.executeQuery();
            // "Get all editions" have same code:
            EditionDAO.getInstance().getAllEditions(list, rs, lang);
            pStatement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return list;
    }

    public void deleteOrder(int userId, int editionId) {
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement ps = connection.prepareStatement(
                    "DELETE FROM orders WHERE "
                            + DBFields.ORDERS_USER_ID + " = ? AND "
                            + DBFields.ORDERS_EDITION_ID + " = ?"
            );
            ps.setInt(1, userId);
            ps.setInt(2, editionId);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
    }
}
