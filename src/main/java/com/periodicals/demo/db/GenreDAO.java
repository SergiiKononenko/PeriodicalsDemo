package com.periodicals.demo.db;

import com.periodicals.demo.db.entity.Genre;
import com.periodicals.demo.db.util.DefineLanguage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GenreDAO {

    private static GenreDAO genreDAO;

    private GenreDAO() {
    }

    public static synchronized GenreDAO getInstance() {
        if (genreDAO == null) {
            genreDAO = new GenreDAO();
        }
        return genreDAO;
    }

    public List<Genre> getAllGenres(String language) {
        String lang = DefineLanguage.getLanguage(language);

        List<Genre> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(
                    "SELECT id, " + DBFields.GENRE_NAME + lang + " FROM genres");
            while (rs.next()) {
                int id = rs.getInt(DBFields.ID);
                String genreName = rs.getString(DBFields.GENRE_NAME + lang);
                list.add(new Genre(id, genreName));
            }
            rs.close();
            statement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }

        return list;
    }

    public boolean createGenre(String englishGenreName, String russianGenreName) {
        Connection connection = null;
        boolean isCreated = false;
        try {
            connection = DBManager.getInstance().getConnection();
            connection.setAutoCommit(false);

            PreparedStatement pStatement = connection.prepareStatement(
                    "INSERT INTO genres (genre_en, genre_ru) VALUES (?, ?)");
            pStatement.setString(1, englishGenreName);
            pStatement.setString(2, russianGenreName);
            pStatement.executeUpdate();

            pStatement.close();
            isCreated = true;
        } catch (SQLException throwable) {
            DBManager.getInstance().rollbackAndClose(connection);
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(connection);
        }
        return isCreated;
    }

    public Genre getGenreByName(String genreName) {
        Genre genre = null;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            PreparedStatement ps = connection.prepareStatement(
                    "SELECT * FROM genres WHERE genre_en = ? OR genre_ru = ?");
            ps.setString(1, genreName);
            ps.setString(2, genreName);

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id = rs.getInt(DBFields.ID);
                String nameEn = rs.getString("genre_en");
                String nameRu = rs.getString("genre_ru");
                genre = new Genre();
                genre.setId(id);
                String name;
                if (nameRu.equals(genreName)) {
                    name = nameRu;
                } else {
                    name = nameEn;
                }
                genre.setGenreName(name);
            }
            rs.close();
            ps.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }

        return genre;
    }

    public boolean isExistsGenreName(String genreName, String language) {
        boolean genreExists = false;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("SELECT " + DBFields.GENRE_NAME + language
                    + " FROM genres WHERE " + DBFields.GENRE_NAME + language + " = '" + genreName + "'");

            if (rs.next()) {
                genreExists = true;
            }
            rs.close();
            statement.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            DBManager.getInstance().quietlyClose(connection);
        }
        return genreExists;
    }

}