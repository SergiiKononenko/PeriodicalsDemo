<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="my-tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="All users" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>

<div class="container">
    <div class="row">
        <c:import url="/WEB-INF/jsp/components/sidebar_menu.jsp"/>
        <div class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h1><fmt:message key="users.page.label"/></h1>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th><fmt:message key="users.page.user_id"/></th>
                        <th><fmt:message key="label.login"/></th>
                        <th><fmt:message key="header.user.balance"/></th>
                        <th><fmt:message key="users.page.blocking"/></th>
                        <th><fmt:message key="users.page.sel_block"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${requestScope.allUsers}" var="user">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.login}</td>
                            <td>${user.balance}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${user.blocked == true}">
                                        <fmt:message key="users.page.table.blocked"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="users.page.table.unblocked"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <form method="post" action="controller">
                                        <%-- Hidden fields to verify current user --%>
                                    <input type="hidden" name="command" value="blockUser">
                                    <input type="hidden" name="userId" value="${user.id}">
                                    <input type="hidden" name="isBlocked" value="${user.blocked}">
                                    <c:choose>
                                        <c:when test="${user.userRole == 1}">
                                            <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1"
                                               aria-disabled="true"><fmt:message
                                                    key="users.page.button.admin"/></a>
                                        </c:when>
                                        <c:when test="${user.blocked == false}">
                                            <button type="submit" class="btn btn-primary btn-warning text-white">
                                                <fmt:message key="users.page.button.block"/>
                                            </button>
                                        </c:when>
                                        <c:otherwise>
                                            <button type="submit" class="btn btn-primary btn-success"><fmt:message
                                                    key="users.page.button.unblock"/>
                                            </button>
                                        </c:otherwise>
                                    </c:choose>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>
