<%@ page import="com.periodicals.demo.web.constant.Path" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>
<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Settings" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>
<div class="container-fluid">
    <div class="row">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"><fmt:message key="header.user.settings"/></h1>
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3"><fmt:message key="settings.page.change_settings"/></h4>
                <form class="needs-validation" method="post" action="controller">
                    <%-- Hidden field for controller --%>
                    <input type="hidden" name="command" value="updateSettings"/>
                    <%-- Current page to redirect --%>
                    <input type="hidden" name="currentPage" value="<%=Path.COMMAND_SETTINGS_PAGE%>"/>

                    <div class="col-md-6 mb-3">
                        <label for="password"><fmt:message key="settings.page.new_password"/></label>
                        <input id="password" type="password" name="password" class="form-control">
                        <%-- If pass invalid, show error msg --%>
                        <c:if test="${not empty requestScope.invalidFieldMessage}">
                            <span class="text-red">${requestScope.invalidFieldMessage}</span>
                        </c:if>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit"><fmt:message
                            key="edit_edition.page.apply_changes"/></button>
                </form>
            </div>
        </main>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>