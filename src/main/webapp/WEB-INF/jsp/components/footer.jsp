<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Footer Start --%>
<div class="p-1 shadow-sm"></div>
<div class="container">
    <footer class="pt-4 my-md-5 pt-md-5">
        <div class="row">
            <div class="col-12 col-md">
                <img class="mb-2" src="image/logo.jpg" alt="logo" width="80" height="80">
                <small class="d-block mb-3 text-muted">© 2020</small>
            </div>
            <div class="col-6 col-md">
                <h5>Features</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Cool stuff</a></li>
                    <li><a class="text-muted" href="#">Random feature</a></li>
                    <li><a class="text-muted" href="#">Team feature</a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>Resources</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Resource</a></li>
                    <li><a class="text-muted" href="#">Resource name</a></li>
                    <li><a class="text-muted" href="#">Another resource</a></li>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Team</a></li>
                    <li><a class="text-muted" href="#">Locations</a></li>
                    <li><a class="text-muted" href="#">Privacy</a></li>
                </ul>
            </div>
        </div>
    </footer>
</div>
<%-- End footer --%>

<%-- End body --%>
</body>
<%-- End HTML --%>
</html>
