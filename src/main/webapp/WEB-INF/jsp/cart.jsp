<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Cart" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>
<div class="container-fluid">
    <div class="row">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"><fmt:message key="header.user.cart"/></h1>
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3"><fmt:message key="cart.page.manage_cart"/></h4>
                <%-- If order done --%>
                <c:if test="${not empty requestScope.orderDone}">
                    <p class="text-green">${requestScope.orderDone}</p>
                </c:if>
                <c:choose>
                    <c:when test="${not empty sessionScope.allInCart}">
                        <%-- If not unought money to subscribe all --%>
                        <c:if test="${not empty requestScope.notEnoughMoney}">
                            <p class="text-red">${requestScope.notEnoughMoney}</p>
                        </c:if>
                        <h5 style="color: green;"><fmt:message key="cart.page.total_cost"/>: $
                            <strong>${sessionScope.cartTotalCost}</strong>
                        </h5>
                        <form method="get" action="controller">
                            <input type="hidden" name="command" value="subscribeAll">
                            <button class="btn btn-primary btn-lg btn-block" type="submit"><fmt:message key="cart.page.subscribe_all"/></button>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <p class="text-red"><fmt:message key="cart.page.nothing_in_the_cart"/></p>
                    </c:otherwise>
                </c:choose>

                <hr class="mb-4">

                <c:forEach items="${sessionScope.allInCart}" var="edition">
                    <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 ">
                        <div class="col p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block mb-2 text-primary"><fmt:message key="index.edition.genre"/>: ${edition.genreName}</strong>
                            <h3 class="mb-0">${edition.edName}</h3>
                            <div class="mb-1 text-muted"><fmt:message key="index.edition.date"/> ${edition.pubDate}</div>
                            <p class="card-text mb-auto">${edition.title}</p>
                        </div>
                        <div class="col-auto d-none d-lg-block d-inline-block">
                            <div class="container pt-3 pb-2 text-center">
                                <div class="card mb-4 shadow-sm">

                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal"><fmt:message key="index.edition.cost"/>${edition.price}</h4>
                                    </div>
                                    <div class="card-body text-center">
                                            <%-- if not enough money --%>
                                        <c:if test="${not empty requestScope.notEnoughMoney}">
                                            <c:if test="${(sessionScope.user.balance - edition.price) < 0}">
                                                <p class="text-red">${requestScope.notEnoughMoney}</p>
                                            </c:if>
                                        </c:if>
                                        <a href="controller?command=subscribeFromCart&editionId=${edition.id}"
                                           class="btn btn-primary btn-success mt-3"><fmt:message key="index.button.subscribe"/></a>
                                        <br>
                                        <a href="controller?command=removeFromCart&editionId=${edition.id}"
                                           class="btn btn-primary btn-warning mt-3 text-white"><fmt:message key="cart.page.remove_from_cart"/>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </main>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>