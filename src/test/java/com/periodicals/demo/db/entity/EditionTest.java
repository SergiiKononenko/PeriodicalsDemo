package com.periodicals.demo.db.entity;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class EditionTest {

    private Edition edition;

    @Before
    public void initVar() {
        edition = new Edition(1, "One", "Description", 22.0, new Date(), "World");
    }

    @Test
    public void setGenreName() {
        edition.setGenreName("Art");
        assertEquals("Art", edition.getGenreName());
    }

    @Test
    public void getId() {
        assertEquals(1, edition.getId());
    }

    @Test
    public void setId() {
        edition.setId(2);
        assertEquals(2, edition.getId());
    }

    @Test
    public void getEdName() {
        assertEquals("One", edition.getEdName());
    }

    @Test
    public void setEdName() {
        edition.setEdName("Name");
        assertEquals("Name", edition.getEdName());
    }

    @Test
    public void getTitle() {
        assertEquals("Description", edition.getTitle());
    }

    @Test
    public void setTitle() {
        edition.setTitle("Description2");
        assertEquals("Description2", edition.getTitle());
    }

    @Test
    public void getPrice() {
        assertEquals(22, edition.getPrice(), 0);
    }

    @Test
    public void setPrice() {
        edition.setPrice(22.2);
        assertEquals(22.2, edition.getPrice(), 0);
    }

    @Test
    public void getPubDate() {
        assertEquals(new Date(), edition.getPubDate());
    }

    @Test
    public void setPubDate() {
        Date date = new Date();
        edition.setPubDate(date);
        assertEquals(date, edition.getPubDate());
    }

    @Test
    public void getGenreName() {
        assertEquals("World", edition.getGenreName());
    }

    @Test
    public void testToString() {
        String expected = "Edition{" +
                "id=" + 1 +
                ", edName='" + "One" + '\'' +
                ", title='" + "Description" + '\'' +
                ", price=" + 22.0 +
                ", pubDate=" + new Date() +
                ", genreName=" + "World" +
                '}';
        assertEquals(expected, edition.toString());
    }
}