package com.periodicals.demo.db.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GenreTest {

    private Genre genre;

    @Before
    public void initVar() {
        genre = new Genre(1, "World");
    }

    @Test
    public void getId() {
        assertEquals(1, genre.getId());
    }

    @Test
    public void setId() {
        genre.setId(2);
        assertEquals(2, genre.getId());
    }

    @Test
    public void getGenreName() {
        assertEquals("World", genre.getGenreName());
    }

    @Test
    public void setGenreName() {
        genre.setGenreName("Genre");
        assertEquals("Genre", genre.getGenreName());
    }

    @Test
    public void testToString() {
        String expected = "Genre{" +
                "id=" + genre.getId() +
                ", genreName='" + genre.getGenreName() + '\'' +
                '}';
        assertEquals(expected, genre.toString());
    }
}