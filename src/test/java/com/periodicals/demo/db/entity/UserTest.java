package com.periodicals.demo.db.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User user;

    @Before
    public void initVar() {
        user = new User(1, "user", "password", 22.2, false, 2);
    }

    @Test
    public void testToString() {
        String expected = "User{" +
                "id=" + user.getId() +
                ", login='" + user.getLogin() + '\'' +
                ", password='" + user.getPassword() + '\'' +
                ", balance=" + user.getBalance() +
                ", isBlocked=" + user.isBlocked() +
                ", userRole=" + user.getUserRole() +
                '}';
        assertEquals(expected, user.toString());
    }

    @Test
    public void getId() {
        assertEquals(1, user.getId());
    }

    @Test
    public void setId() {
        user.setId(2);
        assertEquals(2, user.getId());
    }

    @Test
    public void getLogin() {
        assertEquals("user", user.getLogin());
    }

    @Test
    public void setLogin() {
        user.setLogin("login");
        assertEquals("login", user.getLogin());
    }

    @Test
    public void getPassword() {
        assertEquals("password", user.getPassword());
    }

    @Test
    public void setPassword() {
        user.setPassword("new pass");
        assertEquals("new pass", user.getPassword());
    }

    @Test
    public void getBalance() {
        assertEquals(22.2, user.getBalance(), 0);
    }

    @Test
    public void setBalance() {
        user.setBalance(33.3);
        assertEquals(33.3, user.getBalance(), 0);
    }

    @Test
    public void isBlocked() {
        assertFalse(user.isBlocked());
    }

    @Test
    public void setBlocked() {
        user.setBlocked(true);
        assertTrue(user.isBlocked());
    }

    @Test
    public void getUserRole() {
        assertEquals(2, user.getUserRole());
    }

    @Test
    public void setUserRole() {
        user.setUserRole(1);
        assertEquals(1, user.getUserRole());
    }
}