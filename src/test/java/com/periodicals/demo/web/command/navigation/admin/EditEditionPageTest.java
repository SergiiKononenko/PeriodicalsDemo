//package com.periodicals.demo.web.command.navigation.admin;
//
//import com.periodicals.demo.web.constant.Path;
//import com.periodicals.demo.web.constant.Request;
//import org.junit.Test;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import static org.junit.Assert.*;
//import static org.mockito.Mockito.*;
//
//public class EditEditionPageTest {
//
//    @Test
//    public void execute_if_editionIdStr_null() {
//        HttpServletRequest request = mock(HttpServletRequest.class);
//        HttpServletResponse response = mock(HttpServletResponse.class);
//
//        when(request.getParameter(Request.EDITION_ID)).thenReturn(null);
//
//        String expected = Path.PAGE_EDIT_EDITION;
//
//        assertEquals(expected, new EditEditionPage().execute(request, response));
//        verify(request, times(1)).getParameter(Request.EDITION_ID);
//        verify(request, never()).getSession();
//    }
//}