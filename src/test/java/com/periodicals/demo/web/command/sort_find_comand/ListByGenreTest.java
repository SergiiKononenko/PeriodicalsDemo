package com.periodicals.demo.web.command.sort_find_comand;

import com.periodicals.demo.web.constant.Path;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ListByGenreTest {

    @Test
    public void execute_if_genreName_null_than_do_nothing_and_return_main_page() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter("genreName")).thenReturn(null);
        String expected = Path.PAGE_MAIN;

        assertEquals(expected, new ListByGenre().execute(request, response));
        verify(request, times(1)).getParameter("genreName");
    }
}