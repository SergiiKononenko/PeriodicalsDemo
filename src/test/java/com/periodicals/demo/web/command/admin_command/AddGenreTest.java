package com.periodicals.demo.web.command.admin_command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AddGenreTest {

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;

    @Before
    public void initMocks() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void execute_if_EnglishFieldValid_not_valid() {

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(Request.GENRE_NAME_EN)).thenReturn(null);
        when(request.getParameter(Request.GENRE_NAME_RU)).thenReturn("тест");
        when(session.getAttribute(Session.LANGUAGE)).thenReturn(null);

        String expected = Path.PAGE_ADD_GENRE;

        assertEquals(expected, new AddGenre().execute(request, response));
        verify(request, times(1)).getParameter(Request.GENRE_NAME_EN);
        verify(request, times(1)).getParameter(Request.GENRE_NAME_RU);
    }

}