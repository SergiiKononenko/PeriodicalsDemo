//package com.periodicals.demo.web.command.navigation.admin;
//
//import com.periodicals.demo.web.constant.Path;
//import org.junit.Test;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import static org.junit.Assert.*;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.mockito.Mockito.*;
//
//public class AddGenrePageTest {
//
//    @Test
//    public void execute() {
//        HttpServletRequest request = mock(HttpServletRequest.class);
//        HttpServletResponse response = mock(HttpServletResponse.class);
//
//        String expected = Path.PAGE_ADD_GENRE;
//
//        assertEquals(expected, new AddGenrePage().execute(request, response));
//        verify(request, never()).getParameter(anyString());
//        verify(request, never()).getSession();
//    }
//}