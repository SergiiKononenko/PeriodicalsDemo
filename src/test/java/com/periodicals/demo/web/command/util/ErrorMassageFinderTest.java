package com.periodicals.demo.web.command.util;

import com.periodicals.demo.web.constant.Session;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.ResourceBundle;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ErrorMassageFinderTest {

    private HttpServletRequest request;
    private HttpSession session;

    @Before
    public void initMocks() {
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void getResourceBundle_check_returned_value() {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(null)).thenReturn("");

        ResourceBundle resourceBundle = ErrorMassageFinder.getResourceBundle(request);

        assertTrue(resourceBundle instanceof ResourceBundle);

    }

    @Test
    public void getResourceBundle_if_language_not_install() {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(null)).thenReturn("");

        String result = ErrorMassageFinder.getResourceBundle(request).getString("label.login");
        String expected = "Login";
        assertEquals(expected, result);
    }

    @Test
    public void getResourceBundle_if_language_english() {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(Session.LANGUAGE)).thenReturn("en");

        String result = ErrorMassageFinder.getResourceBundle(request).getString("label.login");
        String expected = "Login";
        assertEquals(expected, result);
    }

    @Test
    public void getResourceBundle_if_language_russian() {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(Session.LANGUAGE)).thenReturn("ru");

        String result = ErrorMassageFinder.getResourceBundle(request).getString("label.login");
        String expected = "Логин";
        assertEquals(expected, result);
    }

}