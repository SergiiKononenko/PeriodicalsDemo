package com.periodicals.demo.web.command.sort_find_comand;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SortByTest {

    @Test
    public void execute() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        List<Edition> editionList = new ArrayList<>();

        when(request.getParameter("sortBy")).thenReturn("name");
        when(request.getSession()).thenReturn(session);

        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(editionList);

        String expectedReturnedValue = Path.PAGE_MAIN;
        SortBy sortBy = new SortBy();

        assertEquals(expectedReturnedValue, sortBy.execute(request, response));

        verify(request, times(1)).getParameter("sortBy");
        verify(request, times(2)).getSession();

    }
}