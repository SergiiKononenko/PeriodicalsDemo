package com.periodicals.demo.web.command.navigation;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class MainPageTest {

    @Test
    public void execute_when_allEditions_not_null() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(new ArrayList<>());
        String expected = Path.PAGE_MAIN;

        assertEquals(expected, new MainPage().execute(request, response));
        verify(request, times(2)).getSession();
        verify(session, times(1)).getAttribute(Session.ALL_EDITIONS);
    }
}