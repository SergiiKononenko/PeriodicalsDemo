package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LogoutCommandTest {

    @Test
    public void execute() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        doNothing().when(session).setAttribute(Session.USER, null);
        String expected = Path.PAGE_MAIN;

        assertEquals(expected, new LogoutCommand().execute(request, response));
    }
}