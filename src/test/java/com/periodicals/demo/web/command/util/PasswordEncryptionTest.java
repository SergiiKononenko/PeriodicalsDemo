package com.periodicals.demo.web.command.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordEncryptionTest {

    @Test
    public void md5String() {
        assertEquals("202cb962ac59075b964b07152d234b70", PasswordEncryption.md5String("123"));
    }
}