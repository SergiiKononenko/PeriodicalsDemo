package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PaginationTest {

    private List<Edition> editionList;

    @Before
    public void initListEditions() {
        editionList = new ArrayList<Edition>() {{
            add(new Edition(1, "One", "Description", 2.0, new Date(), "World"));
            add(new Edition(2, "Two", "Description", 2.0, new Date(), "World"));
            add(new Edition(3, "Three", "Description", 2.0, new Date(), "World"));
            add(new Edition(4, "For", "Description", 2.0, new Date(), "World"));
            add(new Edition(5, "Five", "Description", 2.0, new Date(), "World"));
        }};
    }

    @Test
    public void setUpPagination_if_editionList_not_empty() {
        HttpSession session = mock(HttpSession.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        int qtyEditionsOnPaginationPages = 3;

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(Request.PAGE)).thenReturn(null);

        doNothing().when(session).setAttribute(anyString(), anyString());

        Pagination.setUpPagination(request, editionList);

        List<Edition> foundEditions = Pagination.determinePageToLoad("1", qtyEditionsOnPaginationPages, editionList);

        verify(request, times(1)).getSession();
        verify(request, times(1)).getParameter(Request.PAGE);
        verify(session, times(1)).setAttribute(Request.PAGE_QUANTITY, new int[]{1, 2});
        verify(session, times(1)).setAttribute(Session.PAGINATION_PAGE, "1");
        verify(session, times(1)).setAttribute(Session.FOUND_EDITIONS, foundEditions);
    }

    @Test
    public void setUpPagination_if_editionList_is_empty() {
        HttpSession session = mock(HttpSession.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        editionList = null;

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(Request.PAGE)).thenReturn(null);

        doNothing().when(session).setAttribute(anyString(), anyString());

        Pagination.setUpPagination(request, editionList);

        verify(request, times(1)).getSession();
        verify(request, times(1)).getParameter(Request.PAGE);
        verify(session, times(1)).setAttribute(Request.PAGE_QUANTITY, null);
        verify(session, times(1)).setAttribute(Session.PAGINATION_PAGE, "1");
        verify(session, times(1)).setAttribute(Session.FOUND_EDITIONS, new ArrayList<Edition>());
    }

    @Test
    public void determinePageToLoad() {
        String pageNum = "2";
        int qtyEditionsOnPaginationPages = 3;
        List<Edition> list = Pagination.determinePageToLoad(pageNum, qtyEditionsOnPaginationPages, editionList);
        String editionName = list.get(0).getEdName();
        String expectedName = "For";
        assertEquals(expectedName, editionName);
    }

    @Test
    public void determinePageQuantity() {
        int qtyEditionsOnPaginationPages = 4;
        int[] qtyOfPages = Pagination.determinePageQuantity(qtyEditionsOnPaginationPages, editionList);
        assertEquals(2, qtyOfPages.length);
    }
}