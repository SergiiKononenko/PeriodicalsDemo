package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EditionFinderTest {

    private List<Edition> editionList;
    private Edition editionExists;
    private Edition editionNotExists;

    @Before
    public void initData() {
        editionExists = new Edition(2, "Two", "Description", 5.0, new Date(), "someGenre");
        editionNotExists = new Edition(22, "SomeName", "Description", 5.0, new Date(), "someGenre");
        editionList = new ArrayList<>();
        editionList.add(new Edition(1, "One", "Description", 5.0, new Date(), "someGenre"));
        editionList.add(new Edition(2, "Two", "Description", 5.0, new Date(), "someGenre"));
        editionList.add(new Edition(3, "Tree", "Description", 5.0, new Date(), "someGenre"));
    }

    @Test
    public void getEditionFromListById_if_list_have_one() {
        assertEquals(editionExists, EditionFinder.getEditionFromListById(2, editionList));
    }

    @Test
    public void getEditionFromListById_if_list_doesNotHave_one() {
        int notExistsIdInList = 22;
        assertNull(EditionFinder.getEditionFromListById(notExistsIdInList, editionList));
    }

    @Test
    public void deleteCurrentEditionFromList_if_list_have_one() {
        EditionFinder.deleteCurrentEditionFromList(editionExists, editionList);
        assertEquals(2, editionList.size());
    }

    @Test
    public void deleteCurrentEditionFromList_if_list_doesNotHave_one() {
        EditionFinder.deleteCurrentEditionFromList(editionNotExists, editionList);
        assertEquals(3, editionList.size());
    }

    @After
    public void cleanUpMemoryAfterTests() {
        editionExists = null;
        editionNotExists = null;
        editionList = null;
    }
}