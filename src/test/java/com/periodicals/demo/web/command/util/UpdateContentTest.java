package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.*;

public class UpdateContentTest {

    @Test
    public void updateAllSessionContent() {
        String language = "en";
        List<Edition> editionList = new ArrayList<>();
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(Session.FOUND_EDITIONS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_ORDERS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_IN_CART)).thenReturn(null);

        doNothing().when(session).setAttribute(Session.FOUND_EDITIONS, new ArrayList<>());
        doNothing().when(session).setAttribute(Session.ALL_EDITIONS, new ArrayList<>());
        doNothing().when(session).setAttribute(Session.ALL_ORDERS, new ArrayList<>());
        doNothing().when(session).setAttribute(Session.ALL_IN_CART, new ArrayList<>());

        UpdateContent.updateAllSessionContent(session, language);

        verify(session, times(4)).getAttribute(anyString());
        verify(session, times(1)).setAttribute(Session.FOUND_EDITIONS, editionList);
        verify(session, times(1)).setAttribute(Session.ALL_EDITIONS, editionList);
        verify(session, times(1)).setAttribute(Session.ALL_ORDERS, editionList);
        verify(session, times(1)).setAttribute(Session.ALL_IN_CART, editionList);
    }
}