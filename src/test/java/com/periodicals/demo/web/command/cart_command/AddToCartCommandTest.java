package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Path;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AddToCartCommandTest {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    @Before
    public void initVars() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void execute() {
        Edition currentEdition = new Edition(1, "Edition", "Description", 2.0, new Date(), "World");
        List<Edition> editionList = new ArrayList<Edition>(){{
            add(currentEdition);
        }};

        when(request.getParameter("editionId")).thenReturn("1");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("foundEditions")).thenReturn(editionList);

        when(session.getAttribute("allInCart")).thenReturn(null);
        doNothing().when(session).setAttribute("allInCart", new ArrayList<>());
        doNothing().when(session).setAttribute("cartTotalCost", 2.0);

        String expected = Path.PAGE_MAIN;
        assertEquals(expected, new AddToCartCommand().execute(request, response));

    }
}