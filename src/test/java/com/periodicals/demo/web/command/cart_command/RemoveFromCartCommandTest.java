package com.periodicals.demo.web.command.cart_command;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Path;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RemoveFromCartCommandTest {

    private double cartTotalCost;
    private Edition currentEdition;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;

    @Before
    public void initVars() {
        cartTotalCost = 28.0;
        currentEdition = new Edition(1, "Edition", "Description", 22.0, new Date(), "World");
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void execute() {
        List<Edition> editionList = new ArrayList<Edition>() {{
            add(currentEdition);
        }};
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("allInCart")).thenReturn(editionList);
        when(session.getAttribute("cartTotalCost")).thenReturn(cartTotalCost);
        when(request.getParameter("editionId")).thenReturn("1");
        doNothing().when(session).setAttribute("cartTotalCost", 0);

        String expected = Path.COMMAND_CART_PAGE;
        assertEquals(expected, new RemoveFromCartCommand().execute(request, response));
    }

    @Test
    public void findResidualTotalCost() {
        double inFact = RemoveFromCartCommand.findResidualTotalCost(cartTotalCost, currentEdition);
        assertEquals(6.0, inFact, 0);
    }
}