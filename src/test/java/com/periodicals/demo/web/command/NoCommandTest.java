package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class NoCommandTest {

    @Test
    public void execute() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        doNothing().when(request).setAttribute("errorMessage", "Page not found");

        String expected = Path.PAGE_ERROR_PAGE;

        assertEquals(expected, new NoCommand().execute(request, response));
        verify(request, times(1)).setAttribute("errorMessage", "Page not found");
    }

}