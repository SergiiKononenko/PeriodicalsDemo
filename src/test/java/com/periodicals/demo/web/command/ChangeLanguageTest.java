package com.periodicals.demo.web.command;

import com.periodicals.demo.web.command.navigation.admin.EditEditionPage;
import com.periodicals.demo.web.constant.Path;
import com.periodicals.demo.web.constant.Request;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ChangeLanguageTest {

    private static final String LANGUAGE_PARAM = "?lang=";

    @Test
    public void execute() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(Request.SET_LOCALE)).thenReturn("en");
        doNothing().when(session).setAttribute(Session.LANGUAGE, "en");

        when(session.getAttribute(Session.FOUND_EDITIONS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_ORDERS)).thenReturn(null);
        when(session.getAttribute(Session.ALL_IN_CART)).thenReturn(null);

        String expected = Path.PAGE_MAIN + LANGUAGE_PARAM + "en";

        assertEquals(expected, new ChangeLanguage().execute(request, response));
        verify(request, times(1)).getSession();
        verify(request, times(1)).getParameter(Request.SET_LOCALE);
        verify(session, times(1)).setAttribute(Session.LANGUAGE, "en");
    }
}