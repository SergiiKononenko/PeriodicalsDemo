package com.periodicals.demo.web.command.util;

import com.periodicals.demo.web.constant.Session;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class FieldsValidatorTest {

    private HttpServletRequest request;
    private HttpSession session;

    @Before
    public void initMocks() {
        request = mock(HttpServletRequest.class);
        session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(Session.LANGUAGE)).thenReturn("en");
        doNothing().when(request).setAttribute(anyString(), anyString());
    }

    @Test
    public void isLoginPageFieldsValid_if_login_empty() {
        String login = "";
        String pass = "pass";
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_login_null() {
        String login = null;
        String pass = "pass";
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_password_empty() {
        String login = "login";
        String pass = "";
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_password_null() {
        String login = "login";
        String pass = null;
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_password_less_3_chars() {
        String login = "login";
        String pass = "12";
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_password_mor_10_chars() {
        String login = "login";
        String pass = "12345678910";
        assertFalse(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isLoginPageFieldsValid_if_password_and_login_OK() {
        String login = "login";
        String pass = "pass";
        assertTrue(FieldsValidator.isLoginPageFieldsValid(request, login, pass));
    }

    @Test
    public void isPaymentFieldValid_if_field_invalid_than_false() {
        String balance = "1o";
        assertFalse(FieldsValidator.isPaymentFieldValid(request, balance));
        balance = "10.0.1";
        assertFalse(FieldsValidator.isPaymentFieldValid(request, balance));
        balance = "10.111";
        assertFalse(FieldsValidator.isPaymentFieldValid(request, balance));
    }

    @Test
    public void isPaymentFieldValid_if_field_valid_than_true() {
        String balance = "10";
        assertTrue(FieldsValidator.isPaymentFieldValid(request, balance));
        balance = "10.01";
        assertTrue(FieldsValidator.isPaymentFieldValid(request, balance));
    }

    @Test
    public void isPasswordFieldValid_if_field_invalid_than_false() {
        String pass = "12";
        assertFalse(FieldsValidator.isPasswordFieldValid(request, pass));
        pass = "12345678910";
        assertFalse(FieldsValidator.isPasswordFieldValid(request, pass));
    }

    @Test
    public void isPasswordFieldValid_if_field_valid_than_true() {
        String pass = "10ok";
        assertTrue(FieldsValidator.isPasswordFieldValid(request, pass));
        pass = "10.01ok";
        assertTrue(FieldsValidator.isPasswordFieldValid(request, pass));
    }

    @Test
    public void isGenreEnglishFieldValid_if_empty_than_false() {
        String genreName = "";
        assertFalse(FieldsValidator.isGenreEnglishFieldValid(request, genreName));
        genreName = null;
        assertFalse(FieldsValidator.isGenreEnglishFieldValid(request, genreName));
    }

    @Test
    public void isGenreEnglishFieldValid_if_Cyrillic_present_than_false() {
        String genreName = "gggЛ";
        assertFalse(FieldsValidator.isGenreEnglishFieldValid(request, genreName));
    }

    @Test
    public void isGenreRussianFieldValid_if_empty_than_false() {
        String genreName = "";
        assertFalse(FieldsValidator.isGenreRussianFieldValid(request, genreName));
        genreName = null;
        assertFalse(FieldsValidator.isGenreRussianFieldValid(request, genreName));
    }

    @Test
    public void isGenreRussianFieldValid_if_Latin_present_than_false() {
        String genreName = "ЛЛЛf";
        assertFalse(FieldsValidator.isGenreRussianFieldValid(request, genreName));
    }

}