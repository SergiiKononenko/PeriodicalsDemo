package com.periodicals.demo.web.command.util;

import com.periodicals.demo.db.entity.Edition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class SorterTest {

    private List<Edition> editionList;

    @Before
    public void initData() {
        editionList = new ArrayList<>();
        editionList.add(new Edition(1, "Aaa", "Description", 20.0, new Date(), "someGenre"));
        editionList.add(new Edition(2, "Ccc", "Description", 5.0, new Date(), "someGenre"));
        editionList.add(new Edition(3, "Bbb", "Description", 10.0, new Date(), "someGenre"));
    }

    @Test
    public void sortEditionListByValue_by_name() {
        Sorter.sortEditionListByValue("name", editionList);
        assertEquals("Ccc", editionList.get(2).getEdName());
    }

    @Test
    public void sortEditionListByValue_by_pryce_ascending() {
        Sorter.sortEditionListByValue("priceUp", editionList);
        assertEquals(5, editionList.get(0).getPrice(), 0);
    }

    @Test
    public void sortEditionListByValue_by_descending() {
        Sorter.sortEditionListByValue("priceDown", editionList);
        assertEquals(20, editionList.get(0).getPrice(), 0);
    }

    @After
    public void cleanUpMemoryAfterTests() {
        editionList = null;
    }

}