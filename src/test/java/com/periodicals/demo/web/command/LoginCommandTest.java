package com.periodicals.demo.web.command;

import com.periodicals.demo.web.constant.Path;
import com.sun.deploy.net.HttpRequest;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoginCommandTest {

    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;

    @Before
    public void initVars() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
    }

    @Test
    public void execute_if_login_empty_than_return() {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("login")).thenReturn("");
        when(request.getParameter("password")).thenReturn("password");
        String expected = Path.PAGE_LOGIN;
        assertEquals(expected, new LoginCommand().execute(request, response));
    }

    @Test
    public void execute_if_password_empty_than_return() {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("login")).thenReturn("login");
        when(request.getParameter("password")).thenReturn("");
        String expected = Path.PAGE_LOGIN;
        assertEquals(expected, new LoginCommand().execute(request, response));
    }
}