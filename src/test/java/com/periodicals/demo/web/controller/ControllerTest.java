package com.periodicals.demo.web.controller;

import com.periodicals.demo.db.entity.Edition;
import com.periodicals.demo.web.constant.Session;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class ControllerTest {

    private final static String PATH = "/index.jsp";
    private final static String PARAM_NAME = "command";
    private final static String PARAM_VALUE = "mainPage";

    @Test
    public void doGet() throws ServletException, IOException {
        final Controller controller = new Controller();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(PARAM_NAME)).thenReturn(PARAM_VALUE);
        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(new ArrayList<Edition>());

        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        controller.doGet(request, response);

        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, times(2)).getSession();
        verify(dispatcher).forward(request, response);
    }

    @Test
    public void doPost() throws ServletException, IOException {
        final Controller controller = new Controller();

        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        final HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        when(request.getParameter(PARAM_NAME)).thenReturn(PARAM_VALUE);
        when(session.getAttribute(Session.ALL_EDITIONS)).thenReturn(new ArrayList<Edition>());

        when(request.getRequestDispatcher(PATH)).thenReturn(dispatcher);

        controller.doPost(request, response);

        verify(request, times(1)).getRequestDispatcher(PATH);
        verify(request, times(2)).getSession();
        verify(dispatcher).forward(request, response);
    }
}