package com.periodicals.demo.web.filter;

import com.periodicals.demo.web.constant.Path;
import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class HomePageFilterTest {

    @Test
    public void init() {
    }

    @Test
    public void doFilter() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        doNothing().when(response).sendRedirect(Path.COMMAND_MAIN_PAGE);

        HomePageFilter homePageFilter = new HomePageFilter();
        homePageFilter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(Path.COMMAND_MAIN_PAGE);
        verify(request, never()).getSession();
        verify(filterChain, never()).doFilter(request, response);

    }

    @Test
    public void destroy() {
    }
}