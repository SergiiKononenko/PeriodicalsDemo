package com.periodicals.demo.web.filter;

import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class EncodingFilterTest {

    @Test
    public void init() {
    }

    @Test
    public void doFilter() throws IOException, ServletException {
        final String encoding = "UTF-8";

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        doNothing().when(request).setCharacterEncoding(encoding);
        doNothing().when(response).setCharacterEncoding(encoding);
        doNothing().when(filterChain).doFilter(request, response);

        EncodingFilter encodingFilter = new EncodingFilter();
        encodingFilter.doFilter(request, response, filterChain);

        verify(request, times(1)).setCharacterEncoding(encoding);
        verify(response, times(1)).setCharacterEncoding(encoding);
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void destroy() {
    }
}