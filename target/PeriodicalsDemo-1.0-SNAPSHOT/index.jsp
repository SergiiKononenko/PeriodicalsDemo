<%@ page import="com.periodicals.demo.web.constant.Path" %>
<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="my-tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Main page" scope="request"/>
<%-- Start header --%>
<c:import url="WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>

<div class="container">
    <div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between ml-5 mr-5">
            <a class="p-2 text-blue" href="controller?command=listByGenre&genreName=all"><strong><fmt:message
                    key="genre.all"/></strong></a>
            <my-tag:genres/>
        </nav>
    </div>
    <div class="p-1 mb-3 shadow-sm"></div>

    <%-- This is for change interface logined/unlogined user    --%>
    <c:if test="${sessionScope.user.userRole == 1}">
    <div class="row">
        <c:import url="WEB-INF/jsp/components/sidebar_menu.jsp"/>
            <%--        --%>

        <div class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            </c:if>
            <%-- Editions after sampling --%>
            <c:forEach items="${sessionScope.foundEditions}" var="edition">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 ">

                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-primary"><fmt:message
                                key="index.edition.genre"/>: ${edition.genreName}</strong>
                        <h3 class="mb-0">${edition.edName}</h3>
                        <div class="mb-1 text-muted"><fmt:message key="index.edition.date"/> ${edition.pubDate}</div>
                        <p class="card-text mb-auto">${edition.title}</p>
                    </div>
                    <div class="col-auto d-none d-lg-block d-inline-block">
                        <div class="container pt-3 pb-2 text-center">
                            <div class="card mb-4 shadow-sm">

                                <div class="card-header">
                                    <h4 class="my-0 font-weight-normal"><fmt:message
                                            key="index.edition.cost"/>${edition.price}</h4>
                                </div>
                                <div class="card-body text-center">
                                    <c:choose>

                                        <c:when test="${not empty sessionScope.user}">
                                            <c:choose>
                                                <c:when test="${sessionScope.user.userRole == 1}">
                                                    <a class="btn btn-outline-primary"
                                                       href="controller?command=editEditionPage&editionId=${edition.id}"><fmt:message
                                                            key="index.edition.edit"/></a>
                                                    <a class="btn btn-primary btn-danger"
                                                       href="controller?command=deleteEdition&editionId=${edition.id}"><fmt:message
                                                            key="index.edition.remove"/></a>
                                                </c:when>
                                                <c:when test="${sessionScope.allOrders.contains(edition)}">
                                                    <p class="text-green"><fmt:message
                                                            key="index.edition.subscribed"/></p>
                                                    <a class="btn btn-outline-primary"
                                                       href="<%=Path.COMMAND_MY_SUBSCRIPTIONS%>"><fmt:message
                                                            key="header.user.my_subscriptions"/></a>
                                                </c:when>
                                                <c:when test="${sessionScope.allInCart.contains(edition)}">
                                                    <p><fmt:message key="index.edition.added_to_cart"/></p>
                                                    <a class="btn btn-outline-primary" style="color: darkorange;"
                                                       href="<%=Path.COMMAND_CART_PAGE%>"><fmt:message
                                                            key="index.button.go_to_cart"/></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <%-- if not enough money --%>
                                                    <c:if test="${not empty requestScope.notEnoughMoney}">
                                                        <c:if test="${(sessionScope.user.balance - edition.price) < 0}">
                                                            <p class="text-red">${requestScope.notEnoughMoney}</p>
                                                        </c:if>
                                                    </c:if>
                                                    <a href="controller?command=subscribe&editionId=${edition.id}"
                                                       class="btn btn-primary btn-success mt-3"><fmt:message
                                                            key="index.button.subscribe"/></a>
                                                    <br>
                                                    <a href="controller?command=addToCart&editionId=${edition.id}"
                                                       class="btn btn-primary btn-warning mt-3 text-white"><fmt:message
                                                            key="index.button.add_to_cart"/></a>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>

                                        <c:otherwise>
                                            <a class="btn btn-outline-primary" href="login.jsp"><fmt:message
                                                    key="index.edition.button.login"/></a>
                                        </c:otherwise>

                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>

            <%-- Pagination here --%>
            <c:if test="${not empty sessionScope.pageQuantity}">
                <hr/>
                <ul class="nav nav-pills">
                    <div class="text center" style="margin: 0 auto">
                        <c:forEach items="${sessionScope.pageQuantity}" var="pageNum">
                            <li class="nav-item ml-1 mr-1" style="display: inline-block">
                                <a class="nav-link
                                <c:choose>
                                    <c:when test="${not empty sessionScope.paginationPage}">
                                        <c:if test="${sessionScope.paginationPage == pageNum}">disabled</c:if>
                                    </c:when>
                                    <c:when test="${empty param.page && pageNum == 1}">disabled</c:when>
                                </c:choose>
                                <c:if test="${param.page == pageNum}">disabled</c:if>"
                                   href="controller?command=mainPage&page=${pageNum}">${pageNum}</a>
                            </li>
                        </c:forEach>
                    </div>
                </ul>
            </c:if>

            <%-- This is for change interface logined/unlogined user    --%>
            <c:if test="${sessionScope.user.userRole == 1}">
        </div>
    </div>
    </c:if>

</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>
