<%@ tag import="com.periodicals.demo.db.GenreDAO" %>
<%@ tag import="com.periodicals.demo.db.entity.Genre" %>
<%@ tag import="java.util.List" %>
<%@ tag import="com.periodicals.demo.web.constant.Session" %>
<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>

<% String language = (String) session.getAttribute(Session.LANGUAGE);
    List<Genre> list = GenreDAO.getInstance().getAllGenres(language);
    String genreName;
    for (Genre genre : list) {
        genreName = genre.getGenreName(); %>
        <option value="<%=genreName%>"><%=genreName%></option>
    <% } %>