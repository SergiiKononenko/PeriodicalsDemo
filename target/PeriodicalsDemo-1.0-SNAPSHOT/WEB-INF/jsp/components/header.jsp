<%@ page import="com.periodicals.demo.web.constant.Path" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<!DOCTYPE html>
<%--<fmt:setBundle basename="src.main.resources.resources"/>--%>

<html>
<head>
    <title>${requestScope.title}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/style.css">
</head>
<body>

<header>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal  mb-1"><a href="controller?command=mainPage&main=main"><fmt:message
                key="header.label.periodicals"/></a></h5>
        <form class="my-0 mr-md-auto mb-1" action="controller" method="get">
            <select id="changeLang" name="sortBy" class="custom-select d-block w-100" onchange="this.form.submit()">
                <option value=""><fmt:message key="header.sorter.label"/></option>
                <option value="name"><fmt:message key="header.sorter.by_name"/></option>
                <option value="priceUp"><fmt:message key="header.sorter.by_price_up"/></option>
                <option value="priceDown"><fmt:message key="header.sorter.by_price_down"/></option>
                <%-- hidden fields to send data to handler --%>
                <input type="hidden" name="command" value="sort"/>
            </select>
        </form>

        <form class="my-0 mr-md-auto mb-1" method="get" action="controller">
            <%-- Hidden field: go to DB and search by name --%>
            <input type="hidden" name="command" value="find"/>

            <div class="input-group">
                <input type="text" class="form-control dropdown-toggle" placeholder="<fmt:message key="header.searcher.field"/>"
                       name="editionName">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><fmt:message key="header.searcher.button"/></button>
                </div>
            </div>
        </form>

        <form class="my-0 mr-md-auto mb-1" action="controller" method="get">
            <select name="setLocale" class="custom-select d-block w-100" onchange="this.form.submit()">
                <option value=""><fmt:message key="header.language"/> </option>
                <option value="ru">Русский</option>
                <option value="en">English</option>
                <%-- hidden fields to send data to handler --%>
                <input type="hidden" name="command" value="changeLanguage"/>
            </select>
        </form>

        <c:choose>
            <c:when test="${not empty sessionScope.user}">
                <c:if test="${sessionScope.user.userRole == 2}">
                    <nav class="my-2 my-md-0 mr-md-3">
                    <span style="color: green;"><fmt:message key="header.user.balance"/>
                        <strong>
                            $${sessionScope.user.balance}
                        </strong>
                    </span>
                        <a class="p-2 text-dark" href="<%=Path.COMMAND_REPLENISH_PAGE%>"><fmt:message key="header.user.replenish"/></a>
                        <a class="p-2 text-dark" href="<%=Path.COMMAND_MY_SUBSCRIPTIONS%>"><fmt:message key="header.user.my_subscriptions"/></a>
                        <a class="p-2 text-dark" href="<%=Path.COMMAND_CART_PAGE%>"><fmt:message key="header.user.cart"/></a>
                        <a class="p-2 text-dark" href="<%=Path.COMMAND_SETTINGS_PAGE%>"><fmt:message key="header.user.settings"/></a>
                    </nav>
                </c:if>
                <a class="btn btn-primary btn-info mb-1" href="controller?command=logout"><fmt:message key="button.log_out"/></a>
            </c:when>
            <c:otherwise>
                <a class="btn btn-outline-primary mb-1" href="login.jsp"><fmt:message key="button.sign_in"/></a>
                <a class="btn btn-primary ml-2 mb-1" href="registration.jsp"><fmt:message key="register.page.label"/></a>
            </c:otherwise>
        </c:choose>
    </div>
</header>

<%-- Strart body --%>
<body>