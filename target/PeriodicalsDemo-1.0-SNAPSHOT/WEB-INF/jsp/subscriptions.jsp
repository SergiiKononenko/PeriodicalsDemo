<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Subscriptions" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>
<div class="container-fluid">
    <div class="row">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"><fmt:message key="header.user.my_subscriptions"/></h1>
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3"><fmt:message key="my_subscriptions.page.manage"/></h4>

                <c:choose>
                    <c:when test="${not empty sessionScope.allOrders}">
                        <c:forEach items="${sessionScope.allOrders}" var="edition">
                            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 ">
                                <div class="col p-4 d-flex flex-column position-static">
                                    <strong class="d-inline-block mb-2 text-primary"><fmt:message
                                            key="index.edition.genre"/>: ${edition.genreName}</strong>
                                    <h3 class="mb-0">${edition.edName}</h3>
                                    <div class="mb-1 text-muted"><fmt:message
                                            key="index.edition.date"/> ${edition.pubDate}</div>
                                    <p class="card-text mb-auto">${edition.title}</p>
                                </div>
                                <div class="col-auto d-none d-lg-block d-inline-block">
                                    <div class="container pt-3 pb-2 text-center">
                                        <div class="card mb-4 shadow-sm">
                                            <div class="card-body text-center">
                                                <a href="controller?command=unsubscribe&editionId=${edition.id}"
                                                   class="btn btn-primary btn-danger mt-3"><fmt:message
                                                        key="my_subscriptions.page.unsubscribe"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <p class="text-red"><fmt:message key="my_subscriptions.page.nothing_in_the_sub"/></p>
                    </c:otherwise>
                </c:choose>
                <hr class="mb-4">

            </div>
        </main>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>