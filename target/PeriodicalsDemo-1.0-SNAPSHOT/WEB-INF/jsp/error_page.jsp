<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- HTML + head + header --%>
<c:set var="title" value="Error page" scope="request"/>

<c:import url="/WEB-INF/jsp/components/header.jsp"/>

<%-- Start main --%>
<div class="container text-center">

    <h2 class="h3 mb-3 font-weight-normal"><fmt:message key="error_msg.something_wrong"/></h2>

    <img class="mb-4 text-red" src="/image/error_page.jpg" alt="logo" width="70%">
    <c:if test="${not empty requestScope.errorMessage}">
        <p class="text-red">${requestScope.errorMessage}</p>
    </c:if>

<%-- Start footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>

