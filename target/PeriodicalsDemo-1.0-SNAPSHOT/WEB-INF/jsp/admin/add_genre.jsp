<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="my-tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Add genre" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>

<div class="container">
    <div class="row">
        <c:import url="/WEB-INF/jsp/components/sidebar_menu.jsp"/>
        <div class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h1><fmt:message key="index.sidebar.add_genre"/></h1>
            <hr class="mb-4">


            <div class="col-md-8 order-md-1">
                <%-- If neet to add new genre --%>
                <form class="needs-validation" method="post" action="controller">
                    <%-- Hidden field to to determine handler --%>
                    <input type="hidden" name="command" value="addGenre">

                    <div class="row">

                        <div class="col-md-6 mb-3">
                            <label for="genreEn"><fmt:message key="genre.add_genre_en"/> </label><br>
                            <input id="genreEn" type="text" name="genreNameEn" class="form-control">
                            <c:if test="${not empty requestScope.addGenreErrorMessageEn}">
                                <span class="text-red">${requestScope.addGenreErrorMessageEn}</span>
                            </c:if>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="genreRu"><fmt:message key="genre.add_genre_ru"/> </label><br>
                            <input id="genreRu" type="text" name="genreNameRu" class="form-control">
                            <c:if test="${not empty requestScope.addGenreErrorMessageRu}">
                                <span class="text-red">${requestScope.addGenreErrorMessageRu}</span>
                            </c:if>
                        </div>

                    </div>

                    <button class="btn btn-primary btn-lg btn-block" type="submit"><fmt:message
                            key="index.sidebar.add_genre"/></button>
                </form>

                <hr class="mb-4">
            </div>

        </div>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>