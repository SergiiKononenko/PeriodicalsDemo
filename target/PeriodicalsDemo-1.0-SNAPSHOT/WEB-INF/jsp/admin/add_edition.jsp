<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="my-tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Add edition" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>

<div class="container">
    <div class="row">
        <c:import url="/WEB-INF/jsp/components/sidebar_menu.jsp"/>
        <div class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h1><fmt:message key="add_edition.page.label"/></h1>
            <c:if test="${not empty requestScope.editionExists}">
                <p class="text-red">${requestScope.editionExists}</p>
            </c:if>
            <hr>
            <div class="col-md-8 order-md-1">
                <form class="needs-validation" method="post" action="controller">
                    <%-- Hidden field to to determine handler --%>
                    <input type="hidden" name="command" value="addEdition">

                    <div class="row">

                        <div class="col-md-6 mb-3">
                            <label for="editionNameEn"><fmt:message key="add_edition.page.edition_title_en"/></label><br>
                            <input type="text" name="editionNameEn" class="form-control" id="editionNameEn">
                            <c:if test="${not empty requestScope.addEditionNameErrorMessageEn}">
                                <span class="text-red">${requestScope.addEditionNameErrorMessageEn}</span>
                            </c:if>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="editionNameRu"><fmt:message key="add_edition.page.edition_title_ru"/></label><br>
                            <input type="text" name="editionNameRu" class="form-control" id="editionNameRu">
                            <c:if test="${not empty requestScope.addEditionNameErrorMessageRu}">
                                <span class="text-red">${requestScope.addEditionNameErrorMessageRu}</span>
                            </c:if>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="price"><fmt:message key="add_edition.page.subscription_cost"/></label><br>
                            <input type="number" step="0.01" name="price" class="form-control" id="price"
                                   placeholder="$0.00">
                            <c:if test="${not empty requestScope.addEditionPriceErrorMessage}">
                                <span class="text-red">${requestScope.addEditionPriceErrorMessage}</span>
                            </c:if>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="pubDate"><fmt:message key="index.edition.date"/></label><br>
                            <input type="date" name="date" class="form-control" id="pubDate">
                            <c:if test="${not empty requestScope.addEditionDateErrorMessage}">
                                <span class="text-red">${requestScope.addEditionDateErrorMessage}</span>
                            </c:if>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="titleEn"><fmt:message key="add_edition.page.description_en"/></label><br>
                        <textarea name="titleEn" class="form-control" id="titleEn"></textarea>
                        <c:if test="${not empty requestScope.addEditionTitleErrorMessageEn}">
                            <span class="text-red">${requestScope.addEditionTitleErrorMessageEn}</span>
                        </c:if>
                    </div>
                    <div class="mb-3">
                        <label for="titleRu"><fmt:message key="add_edition.page.description_ru"/></label><br>
                        <textarea name="titleRu" class="form-control" id="titleRu"></textarea>
                        <c:if test="${not empty requestScope.addEditionTitleErrorMessageRu}">
                            <span class="text-red">${requestScope.addEditionTitleErrorMessageRu}</span>
                        </c:if>
                    </div>

                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="genre"><fmt:message key="index.edition.genre"/></label><br>
                            <select name="genre" class="custom-select d-block w-100" id="genre">
                                <option value=""><fmt:message key="add_edition.page.choose"/>...</option>
                                <my-tag:genres-option/>
                            </select>
                            <c:if test="${not empty requestScope.addEditionGenreErrorMessage}">
                                <span class="text-red">${requestScope.addEditionGenreErrorMessage}</span>
                            </c:if>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit"><fmt:message key="add_edition.page.label"/></button>
                </form>

            </div>
        </div>
    </div>
</div>
<%-- End Main --%>

<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>

