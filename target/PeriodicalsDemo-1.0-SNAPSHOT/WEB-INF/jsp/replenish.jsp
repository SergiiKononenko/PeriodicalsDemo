<%@ page import="com.periodicals.demo.web.constant.Path" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- include: --%>
<%-- HTML --%>
<%-- head --%>
<c:set var="title" value="Replenish" scope="request"/>
<%-- Start header --%>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header --%>

<%-- Start main content  --%>
<div class="container-fluid">
    <div class="row">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"><fmt:message key="replenish.page.recharge"/></h1>
            </div>
            <div class="col-md-8 order-md-1">

                <form class="form-signin" method="post" action="controller">
                    <%-- Hidden field for controller --%>
                    <input type="hidden" name="command" value="updateSettings"/>
                    <%-- Current page to redirect --%>
                    <input type="hidden" name="currentPage" value="<%=Path.COMMAND_REPLENISH_PAGE%>"/>

                    <p class="h3 mb-3 font-weight-normal"><fmt:message key="replenish.page.amount"/></p>
                    <br>

                    <%--  if invalid amount error--%>
                    <c:if test="${not empty requestScope.invalidFieldMessage}">
                        <div class="text-red">${requestScope.invalidFieldMessage}</div>
                    </c:if>
                    <div class="col-md-3 mb-3">
                        <label for="replenish" class="sr-only">replenish</label>
                        <input name="amount" type="text" id="replenish" class="form-control dropdown-toggle"
                               placeholder="$0.00">
                    </div>
                    <hr class="mb-4">
                    <button type="submit" class="btn btn-primary btn-success pl-2"><fmt:message
                            key="replenish.page.replenish"/></button>
                </form>
            </div>
        </main>
    </div>
</div>
<%-- End Main --%>


<%-- Start Footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>
<%--  End footer  --%>
<%--  End body  --%>
<%--  End HTML  --%>