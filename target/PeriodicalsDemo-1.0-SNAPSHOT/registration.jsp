<%@ page import="com.periodicals.demo.web.constant.Path" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<c:set var="title" value="Registration page" scope="request"/>
<c:import url="WEB-INF/jsp/components/header.jsp"/>

<%-- Start main --%>
<div class="container pt-3 pb-2 text-center" style="padding: 0 25%; background: #F3FCE8;">

    <form class="form-signin" method="post" action="controller">
        <%--===========================================================================
        Hidden field. In the query it will act as command=register.
        The purpose of this to define the command name, which have to be executed
        after you submit current form.
        ===========================================================================--%>
        <input type="hidden" name="command" value="register"/>
        <%-- current page to redirect --%>
        <input type="hidden" name="currentPage" value="<%=Path.PAGE_LOGIN%>"/>

        <img class="mb-4" src="/image/logo.jpg" alt="logo" width="100" height="100">
        <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="register.page.label"/></h1>

        <%--    if login not unique    --%>
        <c:if test="${not empty requestScope.notUniqueLogin}">
            <p class="text-red">${requestScope.notUniqueLogin}</p>
        </c:if>
        <c:if test="${not empty requestScope.somethingWrongRegMassage}">
            <p class="text-red">${requestScope.somethingWrongRegMassage}</p>
        </c:if>

        <%--    if login error        --%>
        <c:if test="${not empty requestScope.errorMessageLog}">
            <div class="text-red">${requestScope.errorMessageLog}</div>
        </c:if>
        <label for="inputLogin" class="sr-only">Login</label>
        <input name="login" type="text" id="inputLogin" class="form-control" placeholder="<fmt:message key="label.login"/>">
        <br>

        <%--    if passwoed error     --%>
        <c:if test="${not empty requestScope.errorMessagePass}">
            <div class="text-red">${requestScope.errorMessagePass}</div>
        </c:if>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="label.password"/>">
        <br>

        <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="register.page.label"/></button>

        <p class="mt-5 mb-3 text-muted">© 2020</p>
    </form>
</div>
<%-- End main --%>

<c:import url="WEB-INF/jsp/components/footer.jsp"/>
