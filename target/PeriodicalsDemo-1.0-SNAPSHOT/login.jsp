<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Сonnect localization: --%>
<c:if test="${not empty param.lang}">
    <c:set var="language" value="${param.lang}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources"/>

<%-- HTML + head + header --%>
<c:set var="Login page" value="Main page" scope="request"/>
<c:import url="/WEB-INF/jsp/components/header.jsp"/>
<%-- End header--%>

<%-- Start main --%>
<div class="container pt-3 pb-2 text-center" style="padding: 0 25%; background: azure;">

    <form class="form-signin" method="post" action="controller">
        <%--===========================================================================
        Hidden field. In the query it will act as command=login.
        The purpose of this to define the command name, which have to be executed
        after you submit current form.
        ===========================================================================--%>
        <input type="hidden" name="command" value="login"/>

        <img class="mb-4" src="image/logo.jpg" alt="logo" width="100" height="100">
        <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="login.page.label"/></h1>

        <%--    if login not found    --%>
        <c:if test="${not empty requestScope.userNotFound}">
            <p class="text-red">${requestScope.userNotFound}</p>
        </c:if>
        <%--    if registration success    --%>
        <c:if test="${not empty requestScope.registerSuccess}">
            <p style="color: green;">${requestScope.registerSuccess}</p>
        </c:if>

        <%--    if login error        --%>
        <c:if test="${not empty requestScope.errorMessageLog}">
            <div class="text-red">${requestScope.errorMessageLog}</div>
        </c:if>
        <label for="inputLogin" class="sr-only">Login</label>
        <input name="login" type="text" id="inputLogin" class="form-control" placeholder="<fmt:message key="label.login"/>">
        <br>

        <%--    if passwoed error     --%>
        <c:if test="${not empty requestScope.errorMessagePass}">
            <div class="text-red">${requestScope.errorMessagePass}</div>
        </c:if>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="<fmt:message key="label.password"/>">
        <br>

        <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="button.sign_in"/></button>

        <p class="mt-5 mb-3 text-muted">© 2020</p>
    </form>
</div>
<%-- End main --%>

<%-- Start footer --%>
<c:import url="/WEB-INF/jsp/components/footer.jsp"/>

