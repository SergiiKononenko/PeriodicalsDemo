CREATE DATABASE IF NOT EXISTS Periodicals;
USE Periodicals;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS roles;

DROP TABLE IF EXISTS editions;
DROP TABLE IF EXISTS genres;

DROP TABLE IF EXISTS orders;

CREATE TABLE IF NOT EXISTS roles (
	id INT auto_increment primary key,
    role varchar(30) not null unique default 'user'
);

insert into roles (role) values ('admin'), ('user');
select * from roles;

CREATE TABLE IF NOT EXISTS users (
	id INT auto_increment primary key,
    login varchar(30) not null unique,
    pass varchar(50) not null,
    balance double(10, 2),
    is_blocked boolean,
    user_role INT NOT NULL,
    FOREIGN KEY (user_role) REFERENCES roles(id)
		ON DELETE cascade
        ON UPDATE cascade
);

INSERT INTO users (login, pass, balance, is_blocked, user_role)
		   values ('admin', 'admin', 100.00, 0, 1);

SELECT * FROM users WHERE login = 'admin';

UPDATE users SET is_blocked = false WHERE id = 2;
select * from users;

CREATE TABLE IF NOT EXISTS genres (
	id INT auto_increment primary key,
    genre_en varchar(20) not null unique,
    genre_ru varchar(20) not null unique
);

INSERT INTO genres (genre_en, genre_ru) values ('Business', 'Бизнес'), ('Culture', 'Культура'), ('Design', 'Дизайн'), ('Health', 'Здоровье'),
('Politics', 'Политика'), ('Science', 'Наука'), ('World', 'Мир');

select * from genres;
select id, genre_en from genres;
SELECT * FROM genres WHERE genre_en = 'Бизнес' OR genre_ru = 'Бизнес';

SELECT genre_en FROM genres WHERE genre_en = 'Business';

CREATE TABLE IF NOT EXISTS editions (
	id INT auto_increment primary key,
    ed_name varchar(30) not null unique,
    title varchar(300) not null,
	price double (10, 2) not null,
    pub_date date,
    genre_id int,
    FOREIGN KEY (genre_id) REFERENCES genres(id)
		ON DELETE cascade
        ON UPDATE cascade
);

INSERT INTO editions (ed_name, title, price, pub_date, genre_id)
	 VALUES ('Эксмо – АСТ', 'это самая крупная издательская группа в России, занимающая 20% российского книжного рынка после слияния.', 15, 19990812, 1),
			('ОЛМА Медиа', 'российское издательство, занимающее 14% российского книжного рынка.', 25, 20110305, 1),
			('Азбука – Аттикус', 'российское издательство, занимающее 14% российского книжного рынка.', 10, 20080615, 1),
			('Эгмонт Россия', 'дочерняя фирма старейшего в Европе датского издательского концерна «Эгмонт Интернэшнл Холдинг», специализирующаяся на выпуске книг и журналов для детей. ', 18, 20181022, 3),
			('Росмэн', 'детское издательство №1 в России, занимающее 2% российского книжного рынка. Самый известный проект издательства – серия книг Джоан Роулинг о Гарри Поттере. Владелец – Михаил Маркоткин.', 8, 20101101, 3),
			('Экзамен', 'книжное издательство, приоритетным направлением которого являются материалы для подготовки учащихся к ОГЭ и ЕГЭ', 25, 20010201, 3),
			('РИПОЛ классик', 'многопрофильное издательство, генеральным директором которого является Сергей Макаренков. Сотрудничает с иллюстратором Эриком Булатовом. Самая популярная серия книг – “30 секунд”.', 19, 20000101, 3),
			('Феникс', 'крупнейшее региональное издательство России, расположенное в Ростове-на-Дону и выпускающее учебные пособия для ВУЗов и школ.', 30, 20030101, 2),
			('Фламинго', 'издательство, специализирующееся на издании книг для дошкольников и учеников начальных классов (азбуки, буквари, сказки, обучающие книги).', 40, 20130101, 2),
			('Ювента', 'издательство, специализирующееся на учебной и методической литературе для дошкольников, а также для начальной и основной школы.', 11, 19900101, 2);

select * from editions;

UPDATE editions SET ed_name = 'DDDDD' WHERE ed_name = 'Ddddddddd';

select id, ed_name, title, price, pub_date, (select genre from genres where editions.genre_id = genres.id) as genreName from editions;

SELECT id, ed_name, title, price, pub_date, (SELECT genre_ru FROM genres WHERE editions.genre_id = genres.id) AS genreName FROM editions;

-- access to genre
CREATE TABLE IF NOT EXISTS orders (
 user_id INT REFERENCES users(id) ON DELETE CASCADE,
 edition_id INT REFERENCES genres(id) ON DELETE CASCADE,
 UNIQUE (user_id, edition_id)
);

insert into orders (user_id, edition_id) values (2, 2);
insert into orders (user_id, edition_id) values (1, 2);
SELECT * FROM orders;
select * from orders join editions where orders.edition_id = editions.id and user_id = 2;

SELECT id, ed_name, title, price, pub_date, (SELECT genre FROM genres WHERE editions.genre_id = genres.id) AS edition_name
FROM editions
join orders where orders.edition_id = editions.id and user_id = 2;

SELECT id, ed_name, title, price, pub_date, (SELECT genre FROM genres WHERE editions.genre_id = genres.id) AS edition_name
FROM editions join orders where orders.edition_id = editions.id and user_id = ?